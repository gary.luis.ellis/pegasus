package k8s

const (
	JobDeployerServiceAccount = `
apiVersion: v1
kind: ServiceAccount
metadata:
  name: pegasus-job-deployer
  namespace: kube-system
`

	JobDeployerClusterRoleBinding = `
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: pegasus-job-deployer
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  namespace: kube-system
  name: pegasus-job-deployer
`

	AddonJobTemplate = `
  {{- $addonName := .AddonName }}
  {{- $image := .Image }}
  apiVersion: batch/v1
  kind: Job
  metadata:
  {{- if eq .DeleteJob "true" }}
    name: {{$addonName}}-delete-job
  {{- else }}
    name: {{$addonName}}-deploy-job
  {{- end }}
    namespace: kube-system
  spec:
    backoffLimit: 10
    template:
      metadata:
         name: pegasus-deploy
      spec:
          tolerations:
          - key: node-role.kubernetes.io/master
            operator: Exists
            effect: NoSchedule
          - key: node.kubernetes.io/not-ready
            operator: Exists
            effect: NoSchedule
          hostNetwork: true
          serviceAccountName: pegasus-job-deployer
          containers:
            {{- if eq .DeleteJob "true" }}
            - name: {{$addonName}}-delete-pod
            {{- else }}
            - name: {{$addonName}}-pod
            {{- end }}
              image: {{$image}}
              {{- if eq .DeleteJob "true" }}
              command: ["/bin/sh"]
              args: ["-c" ,"kubectl get --ignore-not-found=true -f /etc/config/{{$addonName}}.yaml -o name | xargs kubectl delete --ignore-not-found=true"]
              {{- else }}
              command: [ "kubectl", "apply", "-f" , "/etc/config/{{$addonName}}.yaml"]
              {{- end }}
              volumeMounts:
              - name: config-volume
                mountPath: /etc/config
          volumes:
            - name: config-volume
              configMap:
                # Provide the name of the ConfigMap containing the files you want
                # to add to the container
                name: {{$addonName}}
                items:
                  - key: {{$addonName}}
                    path: {{$addonName}}.yaml
          restartPolicy: Never`
)
