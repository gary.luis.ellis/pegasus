package k8s

import (
	log "github.com/sirupsen/logrus"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

// LabelNode adds the input label map to a kubernetes node
func LabelNode(k8sClient *kubernetes.Clientset, nodeName string, labels map[string]string) error {
	if len(labels) == 0 {
		log.Infof("[k8s/node] %s - label node - no user defined labels are set. Skipping.", nodeName)
		return nil
	}
	log.Infof("[k8s/node] %s -label node - ensuring user defined labels are set.", nodeName)
	node, err := getNode(k8sClient, nodeName)
	if err != nil {
		return err
	}
	nodeLabels := mergeLabels(node.GetLabels(), labels)
	node.SetLabels(nodeLabels)
	_, err = k8sClient.CoreV1().Nodes().Update(node)
	if err != nil {
		log.Errorf("[k8s/node] %s - label node - failed to update node labels. %s", nodeName, err)
		return nil
	}
	log.Infof("[k8s/node] %s - label node - completed.", nodeName)
	return nil
}

func mergeLabels(labels, addlabels map[string]string) map[string]string {
	for k, v := range addlabels {
		labels[k] = v
	}
	return labels
}

func getNode(k8sClient *kubernetes.Clientset, nodeName string) (*corev1.Node, error) {
	node, err := k8sClient.CoreV1().Nodes().Get(nodeName, metav1.GetOptions{})
	if err != nil {
		log.Errorf("[node] get node %s failed. %s", nodeName, err)
		return nil, err
	}
	return node, nil
}
