package k8s

import (
	"reflect"

	v1 "k8s.io/api/core/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"

	log "github.com/sirupsen/logrus"
)

func GetConfigMap(k8sClient *kubernetes.Clientset, configMapName string) (*v1.ConfigMap, error) {
	return k8sClient.CoreV1().ConfigMaps(metav1.NamespaceSystem).Get(configMapName, metav1.GetOptions{})
}

func DeleteConfigMap(k8sClient *kubernetes.Clientset, configMapName string) (*v1.ConfigMap, error) {
	return nil, k8sClient.CoreV1().ConfigMaps(metav1.NamespaceSystem).Delete(configMapName, &metav1.DeleteOptions{})
}

func UpdateConfigMapWithUpdatedFlag(k8sClient *kubernetes.Clientset, configMapName string, configYaml []byte) (bool, error) {
	cfgMap := &v1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Name:      configMapName,
			Namespace: metav1.NamespaceSystem,
			Labels:    commonLabels,
		},
		Data: map[string]string{
			configMapName: string(configYaml),
		},
	}
	updated := false
	existingConfigMap, err := GetConfigMap(k8sClient, configMapName)
	if err != nil {
		if !apierrors.IsNotFound(err) {
			return updated, err
		}

		if _, err := k8sClient.CoreV1().ConfigMaps(metav1.NamespaceSystem).Create(cfgMap); err != nil {
			return updated, err
		}
		return updated, nil
	}
	if !reflect.DeepEqual(existingConfigMap.Data, cfgMap.Data) {
		if _, err := k8sClient.CoreV1().ConfigMaps(metav1.NamespaceSystem).Update(cfgMap); err != nil {
			return updated, err
		}
		updated = true
	}

	return updated, nil
}

// UpdateConfigMap updates the configmap data
func UpdateConfigMap(k8sClient *kubernetes.Clientset, configMapName string, configYaml []byte) error {
	cfgMap := &v1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Name:      configMapName,
			Namespace: metav1.NamespaceSystem,
			Labels:    commonLabels,
		},
		Data: map[string]string{
			configMapName: string(configYaml),
		},
	}

	if _, err := k8sClient.CoreV1().ConfigMaps(metav1.NamespaceSystem).Create(cfgMap); err != nil {
		if !apierrors.IsAlreadyExists(err) {
			//log.Infof("[k8s] configmap %s already exists. updating...", cfgMap.Name)
			return err
		}
		log.Infof("[k8s] configmap %s already exists. updating...", cfgMap.Name)
		if _, err := k8sClient.CoreV1().ConfigMaps(metav1.NamespaceSystem).Update(cfgMap); err != nil {
			return err
		}
	}

	return nil
}

// ApplyConfigMap applies the input configmap yaml manifest
func ApplyConfigMap(k8sClient *kubernetes.Clientset, configMapYaml string) error {
	configMap := v1.ConfigMap{}
	if err := decodeYamlResource(&configMap, configMapYaml); err != nil {
		return err
	}

	if configMap.Namespace == metav1.NamespaceNone {
		configMap.Namespace = metav1.NamespaceSystem
	}

	configMap.Labels = commonLabels

	if _, err := k8sClient.CoreV1().ConfigMaps(configMap.Namespace).Create(&configMap); err != nil {
		if !apierrors.IsAlreadyExists(err) {
			return err
		}
		if _, err := k8sClient.CoreV1().ConfigMaps(configMap.Namespace).Update(&configMap); err != nil {
			return err
		}
	}
	return nil
}
