package k8s

import (
	v1 "k8s.io/api/core/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

func GetSecret(k8sClient *kubernetes.Clientset, secretName string) (*v1.Secret, error) {
	return k8sClient.CoreV1().Secrets(metav1.NamespaceSystem).Get(secretName, metav1.GetOptions{})
}

func DeleteSecret(k8sClient *kubernetes.Clientset, secretName string) (*v1.Secret, error) {
	return nil, k8sClient.CoreV1().Secrets(metav1.NamespaceSystem).Delete(secretName, &metav1.DeleteOptions{})
}

// UpdateSecret updates the secret data
func UpdateSecret(k8sClient *kubernetes.Clientset, secretName string, secretYaml []byte) error {
	secret := &v1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:      secretName,
			Namespace: metav1.NamespaceSystem,
		},
		Data: map[string][]byte{
			secretName: secretYaml,
		},
	}

	secret.Labels = commonLabels

	if _, err := k8sClient.CoreV1().Secrets(metav1.NamespaceSystem).Create(secret); err != nil {
		if !apierrors.IsAlreadyExists(err) {
			return err
		}
		if _, err := k8sClient.CoreV1().Secrets(metav1.NamespaceSystem).Update(secret); err != nil {
			return err
		}
	}

	return nil
}

// ApplySecret applies the input secret yaml manifest
func ApplySecret(k8sClient *kubernetes.Clientset, secretYaml string) error {
	secret := v1.Secret{}
	if err := decodeYamlResource(&secret, secretYaml); err != nil {
		return err
	}

	if secret.Namespace == metav1.NamespaceNone {
		secret.Namespace = metav1.NamespaceSystem
	}

	secret.Labels = commonLabels

	if _, err := k8sClient.CoreV1().Secrets(secret.Namespace).Create(&secret); err != nil {
		if !apierrors.IsAlreadyExists(err) {
			return err
		}
		if _, err := k8sClient.CoreV1().Secrets(secret.Namespace).Update(&secret); err != nil {
			return err
		}
	}
	return nil
}
