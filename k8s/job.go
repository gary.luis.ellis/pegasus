package k8s

import (
	"fmt"
	"time"

	"github.com/sirupsen/logrus"
	v1 "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"

	log "github.com/sirupsen/logrus"
)

type JobStatus struct {
	Completed bool
	Created   bool
}

func ApplyJob(k8sClient *kubernetes.Clientset, jobYaml string, addonUpdated bool) error {
	job := v1.Job{}
	if err := decodeYamlResource(&job, jobYaml); err != nil {
		return err
	}
	if job.Namespace == metav1.NamespaceNone {
		job.Namespace = metav1.NamespaceSystem
	}

	job.Labels = commonLabels

	jobStatus, err := GetK8sJobStatus(k8sClient, job.Name, job.Namespace)
	if err != nil {
		return fmt.Errorf("[k8s/job] Failed to get job complete status for job %s in namespace %s: %v", job.Name, job.Namespace, err)
	}

	if addonUpdated || (jobStatus.Created && !jobStatus.Completed) {
		log.Infof("[k8s/job] replacing job %s", job.Name)
		if err := DeleteK8sSystemJob(jobYaml, k8sClient); err != nil {
			return err
		}
	}
	if _, err = k8sClient.BatchV1().Jobs(job.Namespace).Create(&job); err != nil {
		if apierrors.IsAlreadyExists(err) {
			log.Infof("[k8s/job] Job %s already exists", job.Name)
			return nil
		}
		return err
	}
	log.Infof("[k8s/job] waiting for job %s to complete", job.Name)

	return ensureJobCompleted(k8sClient, job)
}

func DeleteK8sSystemJob(jobYaml string, k8sClient *kubernetes.Clientset) error {
	job := v1.Job{}
	if err := decodeYamlResource(&job, jobYaml); err != nil {
		return err
	}
	if err := deleteK8sJob(k8sClient, job.Name, job.Namespace); err != nil {
		if !apierrors.IsNotFound(err) {
			return err
		}
	} else {
		err = retry(5, 2*time.Second, func() (err error) {
			if err := ensureJobDeleted(k8sClient, job); err != nil {
				return err
			}
			return nil
		})
		if err != nil {
			return err
		}
	}
	return nil
}

func ensureJobCompleted(k8sClient *kubernetes.Clientset, j interface{}) error {
	job := j.(v1.Job)

	jobStatus, err := GetK8sJobStatus(k8sClient, job.Name, job.Namespace)
	if err != nil {
		return fmt.Errorf("[k8s/job] Failed to get job complete status for job %s in namespace %s: %v", job.Name, job.Namespace, err)
	}
	if jobStatus.Completed {
		logrus.Debugf("[k8s/job] Job %s in namespace %s completed successfully", job.Name, job.Namespace)
		return nil
	}
	return fmt.Errorf("[k8s/job] Failed to get job complete status for job %s in namespace %s", job.Name, job.Namespace)
}

func ensureJobDeleted(k8sClient *kubernetes.Clientset, j interface{}) error {
	job := j.(v1.Job)
	_, err := k8sClient.BatchV1().Jobs(job.Namespace).Get(job.Name, metav1.GetOptions{})
	if err != nil {
		if apierrors.IsNotFound(err) {
			return nil
		}
		return err
	}
	return fmt.Errorf("[k8s/job] Job [%s] delete timed out.", job.Name)
}

func deleteK8sJob(k8sClient *kubernetes.Clientset, name, namespace string) error {
	deletePolicy := metav1.DeletePropagationForeground
	var gracePeriodSeconds int64
	gracePeriodSeconds = 120
	return k8sClient.BatchV1().Jobs(namespace).Delete(
		name,
		&metav1.DeleteOptions{
			PropagationPolicy:  &deletePolicy,
			GracePeriodSeconds: &gracePeriodSeconds,
		})
}

func getK8sJob(k8sClient *kubernetes.Clientset, name, namespace string) (*v1.Job, error) {
	return k8sClient.BatchV1().Jobs(namespace).Get(name, metav1.GetOptions{})
}

func GetK8sJobStatus(k8sClient *kubernetes.Clientset, name, namespace string) (JobStatus, error) {
	existingJob, err := getK8sJob(k8sClient, name, namespace)
	if err != nil {
		if apierrors.IsNotFound(err) {
			return JobStatus{}, nil
		}
		return JobStatus{}, err
	}
	for _, condition := range existingJob.Status.Conditions {
		if condition.Type == v1.JobComplete && condition.Status == corev1.ConditionTrue {
			return JobStatus{
				Created:   true,
				Completed: true,
			}, err
		}
	}
	return JobStatus{
		Created:   true,
		Completed: true,
	}, nil
}

// InitJobDeployer creates the pegasus job deployer service account, clusterrole and clusterrole binding.
func InitJobDeployer(k8sClient *kubernetes.Clientset) error {
	log.Infoln("[k8s/jobdeployer] creating job deployer service account configuation")
	log.Debugln("[k8s/jobdeployer] creating job deployer service account")
	if err := UpdatedServiceAccountFromYaml(k8sClient, JobDeployerServiceAccount); err != nil {
		return err
	}

	log.Debugln("[k8s/jobdeployer] creating job deployer cluster role binding")
	if err := UpdateClusterRoleBindingFromYaml(k8sClient, JobDeployerClusterRoleBinding); err != nil {
		log.Errorf("[k8s/jobdeployer] create job deployer cluster role binding failed. %s", err)
		return err
	}
	return nil
}

func retry(attempts int, sleep time.Duration, f func() error) (err error) {
	for i := 0; ; i++ {
		err = f()
		if err == nil {
			return
		}
		if i >= (attempts - 1) {
			break
		}
		time.Sleep(sleep)
		log.Infof("[k8s] retrying after error: %s", err)
	}
	return fmt.Errorf("[k8s] failed after %d attempts", attempts)
}
