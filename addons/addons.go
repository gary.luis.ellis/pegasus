package addons

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/garyellis/pegasus/k8s"
	log "github.com/sirupsen/logrus"
)

type AddOn struct {
	Name string
	Path string
	Url  string
}

func GetManifestFromUrl(url string) ([]byte, error) {
	log.Infof("[addons] fetching addon from %s", url)
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var manifestYaml []byte
	if resp.StatusCode == http.StatusOK {
		manifestYaml, err = ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, err
		}
	}

	return manifestYaml, nil
}

func GetManifestFromFile(fileName string) ([]byte, error) {
	manifestYaml, err := ioutil.ReadFile(fileName)
	if err != nil {
		return nil, err
	}
	return manifestYaml, err
}

func getAddonJob(addonName, image string, isDelete bool) (string, error) {
	jobConfig := map[string]string{
		"AddonName": addonName,
		"Image":     image,
		"DeleteJob": strconv.FormatBool(isDelete),
	}
	return k8s.CompileTemplateFromMap(k8s.AddonJobTemplate, jobConfig)
}

func DoAddOns(kubeConfig string, addOnsImage string, addOns []AddOn) error {
	log.Infoln("[addons] deploying cluster addons")
	k8sClient, err := k8s.NewClient(kubeConfig)
	if err != nil {
		log.Errorf("[addons] configure k8s client failed. %s", err)
		return err
	}

	if err := k8s.InitJobDeployer(k8sClient); err != nil {
		return err
	}

	for _, addon := range addOns {
		log.Debugf("[addons] preparing manifest for %s", addon.Name)
		var manifest []byte
		if addon.Path != "" {
			manifest, err = GetManifestFromFile(addon.Path)
			if err != nil {
				log.Errorf("[addons] read addon from local file failed. %s", err)
				return err
			}
		} else if addon.Url != "" {
			manifest, err = GetManifestFromUrl(addon.Url)
			if err != nil {
				log.Errorf("[addons] read addon from url failed. %s", err)
				return err
			}
		} else {
			log.Warnf("[addons] add-on unrecognized. skipping")
		}
		log.Debugf("[addons] compiled manifest:\n %s", string(manifest))
		log.Infof("[addons] creating jobdeployer configmap %s", addon.Name)
		_, err = k8s.UpdateConfigMapWithUpdatedFlag(k8sClient, fmt.Sprintf("pegasus-addon-%s", addon.Name), manifest)
		if err != nil {
			log.Errorf("[addons] create jobdeployer configmap failed. %s", err)
			return err
		}
		log.Infof("[cluster/addons] create jobdeployer configmap %s succeeded", addon.Name)

		// run the job
		log.Debug("[addons] preparing job manifest")
		jobYaml, err := getAddonJob(fmt.Sprintf("pegasus-addon-%s", addon.Name), addOnsImage, false)
		if err != nil {
			log.Errorf("[addons] setup job %s yaml failed", addon.Name)
			return err
		}
		log.Debugf("[addons] compiled job manifest:\n%s", string(jobYaml))
		err = k8s.ApplyJob(k8sClient, jobYaml, false)
		if err != nil {
			log.Errorf("[addons] create job %s failed. %s", addon.Name, err)
			return err
		}
	}
	log.Infoln("[addons] deploying cluster addons complete.")
	return nil
}
