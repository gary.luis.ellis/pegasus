package dialer

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"

	"github.com/garyellis/pegasus/util/shellutils"

	"github.com/pkg/sftp"
	log "github.com/sirupsen/logrus"
	"github.com/tmc/scp"
	"golang.org/x/crypto/ssh"
)

// Node is a kubernetes cluster node
type Node struct {
	HostName string
	Address  string
	Port     string
	User     string
	Key      string
	Password string
	*Dialer
	Labels map[string]string `mapstructure:"labels,omitempty" json:"labels,omitempty"`
}

// Task is a Command configuration
type Task struct {
	Name           string
	Command        string
	SuppressStdout string
}

// Command represents a command to run on the dialer
type Command struct {
	Name           string
	Command        string
	SuppressStdout string
}

// GetDialer creates the ssh dial config
func (n *Node) CreateDialer(user, keyFile, password string) error {
	n.Dialer = &Dialer{}
	if user != "" {
		n.User = user
	}
	if keyFile != "" {
		n.Key = keyFile
	}
	if password != "" {
		n.Password = password
	}
	n.Dialer.ClientConfig, _ = GetSSHConfig(n)
	return nil
}

// Dialer represents the node ssh configuration
type Dialer struct {
	*ssh.ClientConfig
	*ssh.Client
	*ssh.Session
}

// GetSSHConfig sets up the ssh client config
func GetSSHConfig(n *Node) (*ssh.ClientConfig, error) {
	config := &ssh.ClientConfig{
		User:            n.User,
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}

	if n.Key != "" {
		signer, _ := ParsePrivateKeyFile(n.Key)
		config.Auth = append(config.Auth, ssh.PublicKeys(signer))
	}

	if n.Password != "" {
		config.Auth = append(config.Auth, ssh.Password(n.Password))
	}

	return config, nil
}

// Connect dials the ssh server
func (n *Node) Connect() error {
	n.CreateDialer("", "", "")
	client, err := ssh.Dial("tcp", fmt.Sprintf("%s:%s", n.Address, "22"), n.Dialer.ClientConfig)
	if err != nil {
		return err
	}
	n.Dialer.Client = client
	return nil
}

// ParsePrivateKeyFile validates the input key and returns an ssh.Signer
func ParsePrivateKeyFile(keyfile string) (ssh.Signer, error) {
	key, err := ioutil.ReadFile(keyfile)
	if err != nil {
		log.Fatalf("[dialer] unable to read private key: %v", err)
	}
	signer, err := ssh.ParsePrivateKey(key)
	if err != nil {
		log.Fatalf("[dialer] unable to parse private key: %v", err)
	}
	return signer, nil
}

// Exec creates an ssh session, runs the input command and logs stdout and stdrr streams.
func (n *Node) Exec(cmd string) error {
	session, err := n.Dialer.Client.NewSession()
	if err != nil {
		return err
	}

	defer session.Close()
	var returnStderr []byte
	cmdStdOutReader, _ := session.StdoutPipe()
	cmdStdErrReader, _ := session.StderrPipe()

	log.Debugf("[dialer] [exec] [%s] [%s]", n.Address, cmd)
	err = session.Start(cmd)
	if err != nil {
		log.Errorf("[dialer] start command failed. %s", err)
		return err
	}

	_, _ = shellutils.ReadStdOutErrPipe(ioutil.NopCloser(cmdStdOutReader))
	returnStderr, _ = shellutils.ReadStdOutErrPipe(ioutil.NopCloser(cmdStdErrReader))

	err = session.Wait()
	if err != nil {
		log.Errorf("[dialer] command failed. %s", string(returnStderr))
		return err
	}
	return nil
}

// CopyFile creates an scp session and copies the input byte slice as a file on the remote server
func (n *Node) CopyFile(contents []byte, permission int, remotepath string) error {

	filename := filepath.Base(remotepath)
	tempdir := "/tmp/pegasus-copyfiles"
	temppath := fmt.Sprintf("%s/%s", tempdir, filename)
	dest := filepath.Dir(remotepath)
	destpath := fmt.Sprintf("%s/%s", dest, filename)
	mode := permission

	reader := bytes.NewReader(contents)
	session, err := n.Dialer.Client.NewSession()
	if err != nil {
		return err
	}

	n.Exec(fmt.Sprintf("mkdir -p %s", tempdir))

	// scp.Copy closes the session.
	err = scp.Copy(reader.Size(), os.FileMode(mode), filename, reader, tempdir, session)
	if err != nil {
		return err
	}

	n.Exec(fmt.Sprintf("sudo cp %s %s && rm -f %s", temppath, destpath, temppath))
	return nil
}

// DownloadFile creates an sftp session and downloads the remote file.
func (n *Node) DownloadFile(remotepath, localpath string) error {
	sftpclient, err := sftp.NewClient(n.Dialer.Client)
	if err != nil {
		return err
	}

	defer sftpclient.Close()

	remoteFile, err := sftpclient.Open(remotepath)
	if err != nil {
		return err
	}
	defer remoteFile.Close()

	localFile, err := os.Create(localpath)
	if err != nil {
		return err
	}
	defer localFile.Close()

	_, err = io.Copy(localFile, remoteFile)
	return err
}

// UploadFile creates an sftp session and uploads a file or path to the remote server.
func (n *Node) UploadFile(localpath, remotepath string) error {
	sftpclient, err := sftp.NewClient(n.Dialer.Client)
	if err != nil {
		return err
	}

	defer sftpclient.Close()

	localFile, err := os.Open(localpath)
	if err != nil {
		return err
	}
	defer localFile.Close()

	remoteFile, err := sftpclient.Create(remotepath)
	if err != nil {
		return err
	}
	_, err = io.Copy(remoteFile, localFile)
	return err
}
