# pegasus-cli
Pegasus cli, named after the mythical winged horse, manages Kubernetes cluster lifecycle.


## Features

* Declarative YAML configuration 
* Environment variable substitution on YAML configuration
* Terraform infrastructure provisioning
* K8S AWS Cluster topologies are provided
* BYO Terraform modules is supported
* BYO existing infrastructure is supported
* Kubernetes cluster provisioning with kubeadm
* Kubeadm config api configurable from cli YAML configuration
* Private registries support for air gapped cluster installations
* Upgrades kubernetes with kubeadm upgrade workflows
* User defined cluster addon deployments
* User defined node labeling
* Supports BYO CA certificates
* Cluster CA cert replacement  (kubeadm cert replace workflow) `todo`
* GPG decryption on encrypted certificates and addons `todo`
* Vault Cluster CA bundle creation `todo`
* Vault kv in YAML configuration `todo`
* Docker images synchronization utility (for air gapped deployments)
* Cluster config creation helpers `wip`
* Cluster Deployment project scaffolding utilities `todo`
* 



## Requirements
* Terraform 0.12 when creating infrastructure with cli
* ssh connectivity from cli to cluster nodes destination.
* apiserver connectivity from cli to apiserver endpoint

## Cluster Creation Workflow

* 1\. Infrastructure Provisioning
    * 1.1\. Instantiate terraform root module from config yaml
    * 1.2\. Apply terraform infrastructure
    * 1.3\. Populate cluster nodes information from terraform outputs
    * 1.4\. Terraform provisioning can be optionally skipped when cluster nodes are defined in config yaml
* 2\. Cluster Initialization Control plane nodes
    * 2.1\. Run user defined pre tasks on all cluster nodes
    * 2.2\. When BYO cluster CA is provided, copy CA certs to the init cluster control plane node.
    * 2.3\. Bootstrap cluster on init node.
    * 2.4\. Copy/pull generated cluster certs and keys from the init node.
    * 2.5\. Additional control plane nodes -  upload CA certs and generated certs and keys to 
    * 2.6\. Additional control plane nodes - kubeadm join 
* 3\. Cluster Initialization worker nodes
    * 3.1\. Run user defined pre tasks on all cluster nodes
    * 3.2\. When BYO cluster CA is provided, copy CA certs to the init cluster control plane node.
* 4\. Post cluster creation tasks
    * 4.1\. Applies any user defined labels in infrastructure outputs or in config cluster nodes yaml.
    * 4.2\. Applies user defined addons from list in config yaml



## Config

