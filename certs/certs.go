package certs

import (
	"fmt"
	"path/filepath"

	"github.com/garyellis/pegasus/dialer"
	"github.com/garyellis/pegasus/util/fileutils"

	log "github.com/sirupsen/logrus"
)

const (
	ca                     = "/etc/kubernetes/pki/ca.crt"
	caKey                  = "/etc/kubernetes/pki/ca.key"
	apiserver              = "/etc/kubernetes/pki/apiserver.crt"
	apiserverKey           = "/etc/kubernetes/pki/apiserver.key"
	apiserverEtcdClient    = "/etc/kubernetes/pki/apiserver-etcd-client.crt"
	apiserverEtcdClientKey = "/etc/kubernetes/pki/apiserver-etcd-client.key"
	frontProxyCa           = "/etc/kubernetes/pki/front-proxy-ca.crt"
	frontProxyCaKey        = "/etc/kubernetes/pki/front-proxy-ca.key"
	etcdCa                 = "/etc/kubernetes/pki/etcd/ca.crt"
	etcdKey                = "/etc/kubernetes/pki/etcd/ca.key"
	saPub                  = "/etc/kubernetes/pki/sa.pub"
	saKey                  = "/etc/kubernetes/pki/sa.key"
	certsTarFile           = "kube-certs.tar.gz"
	localDir               = "conf"
	remoteDir              = "/tmp/pegasus"
)

type Certificates struct {
	Ca                     string
	CaKey                  string
	Apiserver              string
	ApiserverKey           string
	ApiserverEtcdClient    string
	ApiserverEtcdClientKey string
	FrontProxyCa           string
	FrontProxyCaKey        string
	SaKey                  string
	SaPub                  string
	EtcdCa                 string
	EtcdCaKey              string
	CertsTarFile           string
	LocalDir               string
	RemoteDir              string
}

// Certs is local certs configuration
type Certs map[string]string

func NewKubeCertificates() *Certificates {
	return &Certificates{
		Ca:                     ca,
		CaKey:                  caKey,
		Apiserver:              apiserver,
		ApiserverKey:           apiserverKey,
		ApiserverEtcdClient:    apiserverEtcdClient,
		ApiserverEtcdClientKey: apiserverEtcdClientKey,
		FrontProxyCa:           frontProxyCa,
		FrontProxyCaKey:        frontProxyCaKey,
		EtcdCa:                 etcdCa,
		EtcdCaKey:              etcdKey,
		SaPub:                  saPub,
		SaKey:                  saKey,
		CertsTarFile:           certsTarFile,
		LocalDir:               localDir,
		RemoteDir:              remoteDir,
	}
}

// UploaExternalCaCerts copies private ca certificates from the local system to the control plane node.
func (c *Certificates) UploaExternalCaCerts(n *dialer.Node, certsCfg *Certs) error {
	log.Infoln("[certs/certs] upload ca certificates to control plane node")
	//if len(*certsCfg) == 0 {
	if certsCfg == nil {
		log.Infoln("[certs/certs] no ca certificates found. Skipping")
		return nil
	}

	var commandstr = `sudo bash -c "mkdir -p $(dirname %s) && cp /tmp/%s %s && rm -f /tmp/%s"`

	for k, v := range *certsCfg {
		f := filepath.Base(v)

		log.Infoln("[certs/certs] uploading", k)
		err := n.UploadFile(v, fmt.Sprintf("/tmp/%s", f))
		if err != nil {
			log.Errorf("[certs/certs] upload ca certificate failed. stderr: %s", err)
			return err
		}

		command := dialer.Command{}
		switch k {
		case "kubecacert":
			command = dialer.Command{
				Name:    fmt.Sprintf("copy %s", k),
				Command: fmt.Sprintf(commandstr, c.Ca, f, c.Ca, f),
			}
		case "kubecakey":
			command = dialer.Command{
				Name:    fmt.Sprintf("copy %s", k),
				Command: fmt.Sprintf(commandstr, c.CaKey, f, c.CaKey, f),
			}
		case "etcdcacert":
			command = dialer.Command{
				Name:    fmt.Sprintf("copy %s", k),
				Command: fmt.Sprintf(commandstr, c.EtcdCa, f, c.EtcdCa, f),
			}
		case "etcdcakey":
			command = dialer.Command{
				Name:    fmt.Sprintf("copy %s", k),
				Command: fmt.Sprintf(commandstr, c.EtcdCaKey, f, c.EtcdCaKey, f),
			}
		case "frontproxycacert":
			command = dialer.Command{
				Name:    fmt.Sprintf("copy %s", k),
				Command: fmt.Sprintf(commandstr, c.FrontProxyCa, f, c.FrontProxyCa, f),
			}
		case "frontproxycakey":
			command = dialer.Command{
				Name:    fmt.Sprintf("copy %s", k),
				Command: fmt.Sprintf(commandstr, c.FrontProxyCaKey, f, c.FrontProxyCaKey, f),
			}
		}
		log.Infof("[certs/certs] running cmd ")
		err = n.Exec(command.Command)
		if err != nil {
			log.Errorf("[certs/certs] Command %s failed. stderr: %s\n", command.Name, err)
			return err
		}
	}

	log.Printf("[certs/certs] Upload external ca certs complete")
	return nil
}

func (c *Certificates) DownloadControlPlaneCerts(n *dialer.Node) error {

	// copy the package certificates script and execute it.
	packagecerts, err := GenPackageCertsScript(c)
	if err != nil {
		log.Fatalln(err)
	}

	log.Infoln("[certs/certs] copying script", packageCertificatesScriptFile)
	n.CopyFile(packagecerts, 0755, packageCertificatesScriptFile)

	commands := []dialer.Command{
		dialer.Command{
			Name:    "package certificates",
			Command: fmt.Sprintf("sudo bash %s %s", packageCertificatesScriptFile, "package"),
		},
	}
	for _, command := range commands {
		log.Printf("[certs/certs] running command %s\n", command.Name)
		err := n.Exec(command.Command)
		if err != nil {
			log.Errorf("[certs/certs] command %s failed. %s\n", command.Name, err)
			log.Fatalln(err)
		}
		log.Printf("[certs/certs] command %s completed", command.Name)
	}

	log.Printf("[certs/certs] downloading certificates \n")
	if err := fileutils.MakeDir(c.LocalDir); err != nil {
		log.Errorf("[certs] failed to create local certs dir %s", c.LocalDir)
		log.Fatalln(err)
	}
	// local path should be a constant and these Download file params cleaned up.
	err = n.DownloadFile(fmt.Sprintf("%s/%s", c.RemoteDir, c.CertsTarFile), fmt.Sprintf("%s/%s", c.LocalDir, c.CertsTarFile))
	if err != nil {
		log.Fatalln(err)
	}
	log.Infof("[certs/certs] download certificates completed. \n")

	return nil
}

func (c *Certificates) UploadControlPlaneCerts(n *dialer.Node) error {

	log.Infoln("[certs/certs] uploading certificates ", packageCertificatesScriptFile)
	packagecerts, err := GenPackageCertsScript(c)
	if err != nil {
		log.Fatalln(err)
	}

	log.Infoln("[certs/certs] uploading script", packageCertificatesScriptFile)
	n.CopyFile(packagecerts, 0755, packageCertificatesScriptFile)

	// local path should be a constant and these Download file params cleaned up.
	log.Infoln("[certs/certs] uploading certificates", packageCertificatesScriptFile)
	err = n.UploadFile(fmt.Sprintf("%s/%s", c.LocalDir, c.CertsTarFile), fmt.Sprintf("/tmp/%s", c.CertsTarFile))
	if err != nil {
		log.Fatalln(err)
	}
	log.Infoln("[certs/certs] upload certificates tarball completed")

	commands := []dialer.Command{
		dialer.Command{
			Name:    "unpack certificates",
			Command: fmt.Sprintf("sudo bash %s %s", packageCertificatesScriptFile, "unpack"),
		},
	}
	for _, command := range commands {
		log.Printf("[certs/certs] %s\n", command.Name)
		err := n.Exec(command.Command)
		if err != nil {
			log.Errorf("[certs/certs] command %s failed. %s\n", command.Name, err)
			log.Fatalln(err)
		}
		log.Printf("[certs/certs] command %s completed", command.Name)
	}

	return nil
}

func (c *Certificates) UploadEtcdCertificates() error {
	return nil
}

func (c *Certificates) DownloadEtcdCertificates() error {
	return nil
}
