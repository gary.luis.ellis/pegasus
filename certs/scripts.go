package certs

import (
	"bytes"
	"text/template"
)

const packageCertificatesScriptFile = "/tmp/package-certs.sh"
const packageCertificatesScriptTmpl = `#!/bin/bash -e

certsTarFile={{.CertsTarFile}}
workDir={{.RemoteDir}}
uploadDir=/tmp

ca="{{.Ca}}"
caKey="{{.CaKey}}"
apiserver="{{.Apiserver}}"
apiserverKey="{{.ApiserverKey}}"
apiserverEtcdClient="{{.ApiserverEtcdClient}}"
apiserverEtcdClientKey="{{.ApiserverEtcdClientKey}}"
frontProxyCa="{{.FrontProxyCa}}"
frontProxyCaKey="{{.FrontProxyCaKey}}"
etcdCa="{{.EtcdCa}}"
etcdKey="{{.EtcdCaKey}}"
saPub={{.SaPub}}
saKey={{.SaKey}}

function log_info(){
    echo "$(date '+%Y-%m-%d %H:%M:%S') INFO [$1] $2"
}

function log_error(){
    echo "$(date '+%Y-%m-%d %H:%M:%S') ERROR [$1] $2"
}

function copy_kube_cert(){
    dest="${workDir}/$(echo ${1}|sed 's@^/etc/kubernetes/@@')"
    log_info "$FUNCNAME" "copying file $1 to $dest"
    if [ ! -f $1 ]; then
        log_error "$FUNCNAME" "file not found: $1"
        exit 1
    fi

	mkdir -p $(dirname $dest)
	cp -p $1 $dest
}

function package_certs(){
	log_info "$FUNCNAME" "packaging $workDir to $workDir/$certsTarFile"
	chmod 755 $workDir
	(cd $workDir && tar czf $certsTarFile .  && chmod 644 $certsTarFile)
}

function package(){
    for i in $ca $caKey $apiserver $apiserverKey $apiserverEtcdClient $apiserverEtcdClientKey $frontProxyCa $frontProxyCaKey $etcdCa $etcdKey $saPub $saKey; do
        copy_kube_cert $i
    done

    package_certs
    log_info "$FUNCNAME" "done"
}

function unpack(){
	log_info "$FUNCNAME" "unpacking $uploadDir/$certsTarFile to /etc/kubernetes"
	pkidir=$(dirname $ca)
	mkdir -p $pkidir
	(cd $pkidir/.. && cp $uploadDir/$certsTarFile . && tar xf $certsTarFile . && chmod 700 $pkidir)
}

function main(){
	log_info "$FUNCNAME" "got action ${1}"
	eval $1
}

main $1
`

const unpackageCertificatesScriptTmpl = `

`

func GenPackageCertsScript(c *Certificates) ([]byte, error) {
	b, err := GenTemplate("package-certs", packageCertificatesScriptTmpl, c)
	if err != nil {
		return nil, err
	}
	return b, nil
}

func GenTemplate(name, tmpl string, c *Certificates) ([]byte, error) {
	out := &bytes.Buffer{}
	t, err := template.New(name).Parse(tmpl)
	if err != nil {
		return nil, err
	}

	if err = t.Execute(out, c); err != nil {
		return nil, err
	}
	return out.Bytes(), nil
}
