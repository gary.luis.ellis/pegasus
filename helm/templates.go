package helm

import (
	"bytes"
	"html/template"

	"github.com/Masterminds/sprig"
)

func getHelmInstallCmTmpl(helmRelease interface{}) ([]byte, error) {
	cfg := map[string]interface{}{
		"HelmRelease": helmRelease,
	}
	return compileTemplateFromMap(helmInstallConfigMapTmpl, cfg)
}

// GetHelmInstalTmpl returns the rendered helm install configmap and job
func getHelmInstalJobTmpl(helmRelease interface{}, image string) ([]byte, error) {
	cfg := map[string]interface{}{
		"HelmRelease": helmRelease,
		"Image":       image,
	}
	return compileTemplateFromMap(helmInstallJobTmpl, cfg)
}

func compileTemplateFromMap(tmplt string, input interface{}) ([]byte, error) {
	out := new(bytes.Buffer)
	t := template.Must(template.New("compiled_template").Funcs(funcMap()).Parse(tmplt))
	if err := t.Execute(out, input); err != nil {
		return nil, err
	}
	return out.Bytes(), nil
}

// funcMaps appends pathToK8sName to spring generic function map
func funcMap() template.FuncMap {
	funcMap := sprig.GenericFuncMap()
	funcMap["dirtok8sdnsname"] = dirToK8sdns123Name
	funcMap["k8sdnsname"] = k8sDns123Hostname
	return template.FuncMap(funcMap)
}

const helmInstallConfigMapTmpl = `
{{- $releaseName := .HelmRelease.ReleaseName }}
{{- $namespace := .HelmRelease.Namespace }}
{{- $repoUrl := .HelmRelease.RepoUrl }}
{{- $chartName := .HelmRelease.ChartName }}
{{- $chartVersion := .HelmRelease.ChartVersion }}
{{- $valuesFiles := .HelmRelease.ValuesFiles }}
{{- $secretValuesFiles := .HelmRelease.SecretValuesFiles }}
{{- $extraArgs := .HelmRelease.ExtraArgs }}
apiVersion: v1
kind: ConfigMap
metadata:
  name: helmrelease-{{ $releaseName }}
  labels:
    app: pegasus-helm
    name:  helmrelease-{{ $releaseName }}
data:
  helminstall.sh: |
    #!/bin/bash -e
    export CHART_REPO_URL={{ $repoUrl }}
    export CHART_REPO_NAME={{ $repoUrl | base }}
    export CHART_NAME={{ $chartName }}
    export CHART_VERSION={{ $chartVersion }}
    export RELEASE_NAME={{ $releaseName }}
    export HELM_EXTRA_ARGS="{{ $extraArgs }}"

    export HELM_HOST=127.0.0.1:44134
    export HELM_TILLER_HISTORY_MAX=10
    export CREATE_NAMESPACE_IF_MISSING=true
    export HELM_TILLER_STORAGE=secret
    export TILLER_NAMESPACE=kube-system

    {{- range $valuesFiles }}
    {{ $valuesFile := . | dirtok8sdnsname }}
    [ -f /{{ $valuesFile }} ] && HELM_ARGS="${HELM_ARGS} -f /{{ $valuesFile }}"
    {{- end}}
    {{- range $secretValuesFiles }}
    {{ $secretValuesFile := . | dirtok8sdnsname }}
    [ -f /{{ $secretValuesFile }} ] && HELM_ARGS="${HELM_ARGS} -f /{{ $secretValuesFile }}"
    {{- end }}
    # pre helm install steps ?
    { tiller --storage=$HELM_TILLER_STORAGE --history-max=$HELM_TILLER_HISTORY_MAX --listen=127.0.0.1:44134 & }
    helm init --client-only
    helm repo add $CHART_REPO_NAME $CHART_REPO_URL
    helm upgrade --install \
      --namespace {{ $namespace }} \
      --version $CHART_VERSION \
      ${HELM_ARGS} \
      $RELEASE_NAME \
      $CHART_REPO_NAME/$CHART_NAME \
      --wait \
      ${HELM_EXTRA_ARGS}
`

const helmInstallJobTmpl = `
{{- $releaseName := .HelmRelease.ReleaseName }}
{{- $repoUrl := .HelmRelease.RepoUrl }}
{{- $valuesFiles := .HelmRelease.ValuesFiles }}
{{- $secretValuesFiles := .HelmRelease.SecretValuesFiles }}
{{- $image := .Image }}
apiVersion: v1/batch
kind: Job
metadata:
  name: pegasus-helm-{{ $releaseName }}
  namespace: kube-system
  labels:
    app: pegasus
    name: helmrelease-{{ $releaseName }}
spec:
  template:
    spec:
      restartPolicy: OnFailure
      serviceAccountName: pegasus-job-deployer
      containers:
      - name: helminstall
        image: {{ $image }}
        command: ["/helminstall.sh"]
        volumeMounts:
        - name: helminstall
          mountPath: /helminstall.sh
          subPath: helminstall.sh
		{{- range $valuesFiles }}
        {{ $valuesFile := . | dirtok8sdnsname }}
        - name: {{ $valuesFile | k8sdnsname }}
          mountPath: /{{ $valuesFile }}
          subPath: {{ $valuesFile }}
        {{- end }}
		{{- range $secretValuesFiles }}
        {{ $secretValuesFile := . | dirtok8sdnsname }}
        - name: {{ $secretValuesFile | k8sdnsname }}
          mountPath: /{{ $secretValuesFile }}
          subPath: {{ $secretValuesFile }}
		{{- end }}
      volumes:
      - name: helminstall
        configMap:
          name: helmrelease-{{ $releaseName }}
          defaultMode: 0755
      {{- range $valuesFiles }}
      {{ $valuesFile := . | dirtok8sdnsname }}
      - name: {{ $valuesFile | k8sdnsname }}
        configMap:
          name: {{ $valuesFile }}
          defaultMode: 0644
      {{- end }}
      {{- range $secretValuesFiles }}
      {{ $secretValuesFile := . | dirtok8sdnsname }}
      - name: {{ $secretValuesFile | k8sdnsname }}
        secret:
          secretName: {{ $secretValuesFile }}
      {{- end }}
`
