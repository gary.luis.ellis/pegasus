package helm

import (
	"fmt"
	"io/ioutil"
	"path"
	"regexp"
	"strings"

	"github.com/garyellis/pegasus/k8s"
	"k8s.io/client-go/kubernetes"

	log "github.com/sirupsen/logrus"
)

var HelmImage string

type HelmRelease struct {
	RepoUrl           string   `mapstructure:"repourl,omitempty"`
	ChartName         string   `mapstructure:"chartname,omitempty"`
	ChartVersion      string   `mapstructure:"chartversion,omitempty"`
	ReleaseName       string   `mapstructure:"releasename,omitempty"`
	Namespace         string   `mapstructure:"namespace,omitempty"`
	ExtraArgs         string   `mapstructure:"extraargs,omitempty"`
	ValuesFiles       []string `mapstructure:"valuesfiles,omitempty"`
	SecretValuesFiles []string `mapstructure:"secretvaluesfiles,omitempty"`
}

// InstallHelmReleases installs the input list of helmreleases
func InstallHelmReleases(k8sclient *kubernetes.Clientset, helmReleases []HelmRelease, image string) error {
	log.Infof("[helm] installing helmreleases")
	if err := k8s.InitJobDeployer(k8sclient); err != nil {
		return err
	}
	for _, helmRelease := range helmReleases {
		if err := helmRelease.Install(k8sclient, image); err != nil {
			return err
		}
	}
	return nil
}

// Install installs helm release secrets, configmaps and job
func (h *HelmRelease) Install(k8sclient *kubernetes.Clientset, image string) error {
	log.Infof("[helm] installing helmrelease %s", h.ReleaseName)
	// create the configmaps from the files list
	if err := createConfigMapsFromFiles(k8sclient, h.ValuesFiles); err != nil {
		return err
	}
	// create the secrets from the files list
	if err := createSecretsFromFiles(k8sclient, h.SecretValuesFiles); err != nil {
		return err
	}

	// setup the helm install configmap
	helmInstallCm, err := getHelmInstallCmTmpl(h)
	if err != nil {
		return err
	}

	log.Debugf("[helm] preparing helm install configmap script: \n%s", string(helmInstallCm))
	log.Infof("[helm] creating %s helm install configmap script", h.ReleaseName)

	if err := k8s.ApplyConfigMap(k8sclient, string(helmInstallCm)); err != nil {
		return err
	}
	log.Infof("[helm] done")

	// apply the helm install job
	helmInstallJob, err := getHelmInstalJobTmpl(h, image)
	if err != nil {
		return err
	}

	log.Debugf("[helm] preparing helm install job: \n%s", string(helmInstallJob))
	log.Infof("[helm] creating helm install job: %s", h.ReleaseName)
	if err := k8s.ApplyJob(k8sclient, string(helmInstallJob), true); err != nil {
		return err
	}
	log.Infof("[helm] done")
	return nil
}

// CreateSecretsFromFiles creates secrets from the list of SecretValuesFiles.
func createSecretsFromFiles(k8sclient *kubernetes.Clientset, paths []string) error {
	for _, s := range paths {
		log.Infof("[helm] creating secret from file %s ", s)
		if err := createSecretFromFile(k8sclient, s); err != nil {
			return err
		}
		log.Infof("[helm] done")
	}
	return nil
}

// CreateConfigMapsFromFiles creates configmaps from the list of ValuesFiles
func createConfigMapsFromFiles(k8sclient *kubernetes.Clientset, paths []string) error {
	for _, c := range paths {
		log.Infof("[helm] creating configmap from file %s ", c)
		if err := createConfigMapFromFile(k8sclient, c); err != nil {
			return err
		}
		log.Infof("[helm] done")
	}
	return nil
}

func createSecretFromFile(k8sclient *kubernetes.Clientset, path string) error {
	name, data, err := getValues(path)
	if err != nil {
		return err
	}
	log.Debugf("[helm] secret file contents: \n%s", string(data))
	if err := k8s.UpdateSecret(k8sclient, name, data); err != nil {
		return err
	}
	return nil
}

func createConfigMapFromFile(k8sclient *kubernetes.Clientset, path string) error {
	name, data, err := getValues(path)
	if err != nil {
		return err
	}
	log.Debugf("[helm] configmap file contents: \n%s", string(data))
	if err := k8s.UpdateConfigMap(k8sclient, name, data); err != nil {
		return err
	}
	return nil
}

func getValues(path string) (string, []byte, error) {
	name := dirToK8sdns123Name(path)
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return "", nil, err
	}
	return name, data, nil
}

// dirToK8sdns123Name formats the input string's directory to lowercase alphanumberic characters
func dirToK8sdns123Name(p string) string {
	basename := path.Base(p)
	dirname := path.Dir(p)

	dirname = k8sDns123Hostname(dirname)
	return fmt.Sprintf("%s-%s", dirname, basename)
}

// k8sDns123Hostname formats the input string to lowercase alphanumberic characters
func k8sDns123Hostname(name string) string {
	reg, _ := regexp.Compile("[/.]")
	return strings.ToLower(reg.ReplaceAllString(name, "-"))
}
