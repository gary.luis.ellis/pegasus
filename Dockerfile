FROM golang:1.12.4 AS build
LABEL maintainer="Gary Ellis <gary.luis.ellis@gmail.com>"
# https://github.com/alextanhongpin/go-docker-multi-stage-build

WORKDIR /go/src/github.com/garyellis/pegasus
RUN go get -u github.com/golang/dep/cmd/dep

COPY . /go/src/github.com/garyellis/pegasus
RUN dep ensure
RUN package=github.com/garyellis/pegasus/cmd VERSION=v0.2.3 && \
    BUILD_DATE="-X '${package}.BuildDate=$(date)'" && \
    GIT_COMMIT="-X ${package}.GitCommit=$(git rev-list -1 HEAD)" && \
    _VERSION="-X ${package}.Version=$VERSION" && \
    FLAGS="$GIT_COMMIT $_VERSION $BUILD_DATE" && \
    export GOOS=linux GOARCH=amd64 && go build -o pegasus-${VERSION}_${GOOS}-${GOARCH} -ldflags "${FLAGS}" && \
    export GOOS=darwin GOARCH=amd64 && go build -o pegasus-${VERSION}_${GOOS}-${GOARCH} -ldflags "${FLAGS}"

FROM ubuntu
COPY --from=build /go/src/github.com/garyellis/pegasus/pegasus* /release/


## TODO
# - parameterize version as a docker build arg. makefile to create version from branch+date or tag+master.
# - consider parameterizing GIT_COMMIT and BUILD_DATE as docker build args.
# - parameterize GOOS and add build GOOS as makefile steps
# - package a separate container release that packages terraform
