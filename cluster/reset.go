package cluster

import (
	"github.com/garyellis/pegasus/config"
	"github.com/garyellis/pegasus/kubeadm"
	log "github.com/sirupsen/logrus"
)

// Reset Resets the kubernetes cluster nodes
func Reset(c *config.Config, skipInfraProvision bool) error {
	cluster := NewCluster(c)

	if !skipInfraProvision {
		reconcileTerraform(c)
	}

	if err := configureNodeDialers(c); err != nil {
		return err
	}

	err := cluster.ResetCluster()
	if err != nil {
		return err
	}
	return nil
}

// ResetCluster runs kubeadm reset on all nodes.
func (c *Cluster) ResetCluster() error {
	// we should setup a function to collect all nodes ordered by worker, control plane and etcd
	workerNodes := c.Worker.Nodes
	controlPlaneNodes := c.ControlPlane.Nodes
	// etcdNodes := c.Etcd.Nodes

	for _, workerNode := range workerNodes {
		log.Infof("[reset] Resetting worker [%s]", workerNode.Address)

		// ssh connect to the server
		if err := workerNode.Connect(); err != nil {
			log.Fatalf("[reset] Ssh connection to [%s] failed. %s", workerNode.Address, err)
		}

		defer workerNode.Dialer.Client.Close()

		err := kubeadm.KubeAdmReset(&workerNode)
		if err != nil {
			log.Fatalf("[reset] exec failed on [%s]. %s", workerNode.Address, err)
		}
	}

	for _, controlPlaneNode := range controlPlaneNodes {
		log.Infof("[reset] Resetting control plane node [%s]", controlPlaneNode.Address)

		// ssh connect to the server
		if err := controlPlaneNode.Connect(); err != nil {
			log.Fatalf("[reset] Ssh connection to [%s] failed. %s", controlPlaneNode.Address, err)
		}

		defer controlPlaneNode.Dialer.Client.Close()

		err := kubeadm.KubeAdmReset(&controlPlaneNode)
		if err != nil {
			log.Fatalf("[reset] exec failed on [%s]. %s", controlPlaneNode.Address, err)
		}
	}

	log.Infof("[reset] Cluster reset completed.")

	return nil
}
