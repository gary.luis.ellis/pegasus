package cluster

import (
	"github.com/garyellis/pegasus/config"
	"github.com/garyellis/pegasus/k8s"
	log "github.com/sirupsen/logrus"
)

// LabelNodes adds user defined labels to cluster nodes
func LabelNodes(kubeConfig string, c *config.Config) error {
	log.Infof("[cluster/nodes] labeling cluster nodes.")
	nodes := collectNodes(c)
	k8sClient, err := k8s.NewClient(kubeConfig)
	if err != nil {
		log.Errorf("[cluster/nodes] unable to setup kubernetes client. %s", err)
		return err
	}
	for _, n := range nodes {
		if err := k8s.LabelNode(k8sClient, n.HostName, n.Labels); err != nil {
			log.Errorf("[cluster/nodes] failed to add labels to node %s. %s", n.HostName, err)
		}
	}
	log.Infof("[cluster/nodes] labeling cluster nodes completed.")
	return nil
}
