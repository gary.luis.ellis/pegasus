package cluster

import (
	"github.com/garyellis/pegasus/config"
	"github.com/garyellis/pegasus/dialer"
	"github.com/garyellis/pegasus/terraform"

	log "github.com/sirupsen/logrus"
)

type Cluster struct {
	*config.Config
}

func NewCluster(c *config.Config) *Cluster {
	return &Cluster{
		Config: c,
	}
}

// CopyFiles copies files to all nodes
func (c *Cluster) CopyFiles() error {
	var nodes []dialer.Node
	errc := make(chan error)
	for _, file := range c.Files {
		log.Infof("[cluster] starting file copy for %s", file.Name)
		// resolve the nodes for the file
		nodes := collectFilteredNodes(c.Config, file.NodeLabels)
		for _, node := range nodes {
			log.Infof("[cluster] [%s] copy file %s", node.Address, file.Dest)
			go func(f config.File, n dialer.Node) {
				if err := n.Connect(); err != nil {
					log.Errorf("[cluster] [%s] ssh failed. %s", n.Address, err)
					errc <- err
					return
				}
				defer n.Dialer.Client.Close()

				log.Infof("[cluster] [%s] file copy %s to %s ", n.Address, f.Name, f.Dest)
				if err := n.CopyFile([]byte(f.Content), f.Permission, f.Dest); err != nil {
					log.Errorf("[cluster] [%s] file copy %s failed. %s", n.Address, f.Name, err)
					errc <- err
					return
				}
				log.Infof("[cluster] [%s] file copy completed", n.Address)
				errc <- nil
			}(file, node)
		}
	}

	for range nodes {
		if err := <-errc; err != nil {
			log.Errorf("[cluster] file copy failed.")
			return err
		}
	}

	return nil
}

// RunPreTasks runs list of pre tasks on all nodes
func (c *Cluster) RunPreTasks() error {
	nodes := collectNodes(c.Config)
	errc := make(chan error)

	for _, node := range nodes {
		log.Infof("[cluster] [%s] running pre tasks...", node.Address)

		go func(n dialer.Node) {
			if err := n.Connect(); err != nil {
				log.Errorf("[cluster] [%s] ssh failed. %s", n.Address, err)
				errc <- err
				return
			}
			defer n.Dialer.Client.Close()

			for _, task := range c.PreTasks {
				log.Infof("[cluster] [%s] running pre-task [%s]", n.Address, task.Name)
				if err := n.Exec(task.Command); err != nil {
					log.Errorf("[cluster] [%s] pre task [%s] failed. %s", n.Address, task.Name, err)
					errc <- err
					return
				}
			}
			log.Infof("[cluster] [%s] pre-tasks completed", n.Address)
			errc <- nil
		}(node)
	}

	for range nodes {
		if err := <-errc; err != nil {
			log.Errorf("[cluster] pre tasks failed.")
			return err
		}
	}

	return nil
}

func configureNodeDialers(c *config.Config) error {
	etcdNodes, err := configureDialers(c.SSHConfig, c.Etcd.Nodes)
	if err != nil {
		return err
	}
	c.SetEtcdNodes(etcdNodes)

	controlPlaneNodes, err := configureDialers(c.SSHConfig, c.ControlPlane.Nodes)
	if err != nil {
		return err
	}
	c.SetControlPlaneNodes(controlPlaneNodes)

	workerNodes, err := configureDialers(c.SSHConfig, c.Worker.Nodes)
	if err != nil {
		return err
	}
	c.SetWorkerNodes(workerNodes)
	return nil
}

// configureDialers needs to return nodes back to caller
func configureDialers(s config.SSHConfig, n []dialer.Node) ([]dialer.Node, error) {
	var nodes []dialer.Node
	for _, node := range n {
		if (config.SSHConfig{} == s) {
			log.Infof("[cluster] setting dialer cfg from the node - user: %s, privateKey: %s", node.User, node.Key)
			err := node.CreateDialer("", "", "")
			if err != nil {
				return nil, err
			}
		} else {
			log.Infof("[cluster] setting dialer cfg from ssh config - user: %s, privatekey: %s password: %s", s.User, s.PrivateKey, "****")
			err := node.CreateDialer(s.User, s.PrivateKey, s.Password)
			if err != nil {
				return nil, err
			}
		}
		nodes = append(nodes, node)
	}
	return nodes, nil
}

func reconcileTerraform(c *config.Config) error {
	var etcdNodes, controlPlaneNodes, workerNodes []dialer.Node
	etcdNodes, controlPlaneNodes, workerNodes, err := terraform.Create(c.Terraform.Modules)
	if err != nil {
		return err
	}
	log.Infoln("[cluster] etcd nodes:", etcdNodes)
	log.Infoln("[cluster] controlPlane nodes:", controlPlaneNodes)
	log.Infoln("[cluster] worker nodes:", workerNodes)

	c.AddEtcdNodes(etcdNodes)
	c.AddControlPlaneNodes(controlPlaneNodes)
	c.AddWorkerNodes(workerNodes)
	return nil
}

func collectNodes(c *config.Config) []dialer.Node {
	log.Infof("[cluster] collecting nodes")
	var nodes []dialer.Node
	nodes = append(nodes, c.Etcd.Nodes...)
	nodes = append(nodes, c.ControlPlane.Nodes...)
	nodes = append(nodes, c.Worker.Nodes...)
	return nodes
}

// collectFilteredNodes returns a []dialer.Node filtered by node label keys.
func collectFilteredNodes(c *config.Config, labels []string) []dialer.Node {
	if len(labels) == 0 {
		return collectNodes(c)
	}
	var nodes []dialer.Node
	for _, node := range collectNodes(c) {
		for _, l := range labels {
			if _, ok := node.Labels[l]; ok {
				log.Infof("[cluster] matched label %s on %s", l, node.Address)
				nodes = append(nodes, node)
			}
		}
	}
	return nodes
}
