package cluster

import (
	"strings"

	"github.com/garyellis/pegasus/addons"

	"github.com/garyellis/pegasus/certs"
	"github.com/garyellis/pegasus/config"
	"github.com/garyellis/pegasus/k8s"

	"github.com/garyellis/pegasus/helm"
	"github.com/garyellis/pegasus/kubeadm"

	log "github.com/sirupsen/logrus"
)

const (
	kubeAdmInitConf     = "/tmp/kubeadm-init-config.yaml"
	kubeAdmJoinConf     = "/tmp/kubeadm-join-config.yaml"
	localKubeConfigConf = "conf/admin.conf"
)

// Create creates the cluster
func Create(c *config.Config, skipInfraProvision bool) error {
	cluster := NewCluster(c)

	if !skipInfraProvision {
		reconcileTerraform(c)
	}

	log.Infoln("[cluster/create] configuring node dialers")
	if err := configureNodeDialers(c); err != nil {
		return err
	}

	if err := cluster.RunPreTasks(); err != nil {
		return err
	}

	// copy files
	if err := cluster.CopyFiles(); err != nil {
		return err
	}

	// create the cluster
	if err := cluster.CreateCluster(); err != nil {
		return err
	}

	// todo - instantiate the k8sclient once and pass it to functions as needed.
	//      - update the conf name to admin_<config>.yaml or set it in config yaml

	// setup user defined node labels
	if err := LabelNodes("./conf/admin.conf", c); err != nil {
		return err
	}

	// placeholder for taints

	// deploy add-ons
	if err := addons.DoAddOns("./conf/admin.conf", c.AddOnsImage, c.AddOns); err != nil {
		return err
	}

	// helmreleases
	k8sClient, err := k8s.NewClient("./conf/admin.conf")
	if err != nil {
		return err
	}
	if err := helm.InstallHelmReleases(k8sClient, c.HelmReleases, c.HelmImage); err != nil {
		return err
	}

	return nil
}

func (c *Cluster) CreateCluster() error {
	err := c.CreateEtcd()
	if err != nil {
		log.Fatalln(err)
	}

	// placeholder to fetch the etcd client certificates if applicable

	err = c.CreateControlPlane()
	if err != nil {
		log.Fatalln(err)
	}

	err = c.CreateWorker()
	if err != nil {
		log.Fatalln(err)
	}

	return nil
}

// CreateEtcd bootstraps the kubernetes etcd nodes.
func (c *Cluster) CreateEtcd() error {
	if len(c.Etcd.Nodes) >= 1 {
		log.Infoln("[cluster/create] bootstrap etcd nodes - not yet implemented")
	}

	return nil
}

// CreateControlPlane bootstraps the kubernetes control plane nodes.
func (c *Cluster) CreateControlPlane() error {

	if len(c.ControlPlane.Nodes) >= 1 {
		initNode := c.ControlPlane.Nodes[0]
		log.Printf("[cluster/create] bootstrapping first control plane node [%s]\n", initNode.Address)

		// ssh to the control plane node
		if err := initNode.Connect(); err != nil {
			log.Fatalf("[cluster/create] Failed to ssh into to [%s]. %s\n", initNode.Address, err)
		}

		defer initNode.Dialer.Client.Close()

		// upload the ca certificates
		controlPlaneCerts := certs.NewKubeCertificates()
		err := controlPlaneCerts.UploaExternalCaCerts(&initNode, c.Config.Certs)
		if err != nil {
			log.Fatalln(err)
		}

		clusterConfig, initConfig := c.Cluster.ClusterConfiguration, c.Cluster.InitConfiguration
		kubeadmInitYaml, err := kubeadm.GetInitControlPlaneTmpl(clusterConfig, initConfig, c.ControlPlane.Nodes, initNode)
		if err != nil {
			log.Errorf("[cluster/create] failed to render template")
			log.Fatalf("[cluster/create] %s", err)
		}
		log.Debugf("[cluster/create] cluster configuration yaml:\n %s", kubeadmInitYaml)

		if !kubeadm.IsControlPlaneRunning(&initNode) {
			err = kubeadm.KubeAdmInit(&initNode, kubeadmInitYaml, kubeAdmInitConf)
			if err != nil {
				log.Fatalf("[cluster/create] exec failed on [%s] %s", initNode.Address, err)
			}
			log.Infof("[cluster/create] Bootstrapped first control plane node successfully")
		}

		// download the admin conf
		kubeadm.GetAdminKubeConf(&initNode, localKubeConfigConf)
		// download the certificates
		controlPlaneCerts.DownloadControlPlaneCerts(&initNode)

		// get the cacert hash pin
		log.Infof("[cluster/create] getting cacerthash pins")
		cacertHash, err := kubeadm.GetCACertHash(localKubeConfigConf)
		if err != nil {
			log.Errorf("[cluster/create] get ca cert hashes failed")
		}
		log.Infof("[cluster/create] got %s", strings.Join(cacertHash, " "))

		// join the remaining control plane nodes
		if len(c.ControlPlane.Nodes) > 1 {

			for _, node := range c.ControlPlane.Nodes[1:] {
				log.Infof("[cluster/create] joining control plane node %s", node.Address)

				if err := node.Connect(); err != nil {
					log.Fatalf("[cluster/create] Failed to ssh into to [%s]. %s", node.Address, err)
				}

				defer node.Dialer.Client.Close()

				// copy the cluster ca certificates
				if !kubeadm.IsControlPlaneRunning(&node) {
					controlPlaneCerts.UploadControlPlaneCerts(&node)

					// get the join yaml config
					kubeadmJoinYaml, err := kubeadm.GetJoinControlPlaneYaml(clusterConfig, initConfig, node, cacertHash)
					if err != nil {
						log.Fatalf("[cluster/create] exec failed on [%s] %s", node.Address, err)
					}
					err = kubeadm.KubeAdmJoin(&node, kubeadmJoinYaml, kubeAdmJoinConf)
					if err != nil {
						log.Infof("[cluster/create] failed to join node")
						log.Fatalf("[cluster/create] %s", err)
					}
				}
			}
		}
	}

	return nil
}

// CreateWorker bootstraps the kubernetes worker.
func (c *Cluster) CreateWorker() error {
	log.Infof("[cluster/create] Join worker nodes.")

	workerNodes := c.Worker.Nodes
	for _, node := range workerNodes {
		log.Infof("[cluster/create] joining worker [%s] to the cluster.", node.Address)
		if err := node.Connect(); err != nil {
			log.Fatalf("[cluster/create] Failed to ssh into node [%s]. %s\n", node.Address, err)
		}

		defer node.Dialer.Client.Close()

		clusterConfig, initConfig := c.Cluster.ClusterConfiguration, c.Cluster.InitConfiguration
		cacertHash, err := kubeadm.GetCACertHash(localKubeConfigConf)
		if err != nil {
			log.Errorf("[cluster/create] failed to get cacert hash pin")
			log.Fatalf("[cluster/create] %s", err)
		}
		kubeadmJoinYaml, err := kubeadm.GetJoinWorkerYaml(clusterConfig, initConfig, node, cacertHash)
		if err != nil {
			log.Errorf("[cluster/create] failed to get join worker yaml")
			log.Fatalf("[cluster/create] %s", err)
		}

		err = kubeadm.KubeAdmJoin(&node, kubeadmJoinYaml, kubeAdmJoinConf)
		if err != nil {
			log.Errorf("[cluster/create] Join worker failed on [%s]", node.Address)
			log.Fatalf("[cluster/create] %s", err)
		}
		log.Infof("[cluster/create] Join worker [%s] succeeded.", node.Address)
	}
	log.Infof("[cluster/create] Join worker nodes completed.")
	return nil
}
