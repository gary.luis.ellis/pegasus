package cluster

import (
	"github.com/garyellis/pegasus/config"
	"github.com/garyellis/pegasus/kubeadm"
	log "github.com/sirupsen/logrus"
)

// Upgrade the cluster
func Upgrade(c *config.Config, skipInfraProvision bool) error {
	log.Infof("[cluster] upgrading cluster")
	cluster := NewCluster(c)

	if !skipInfraProvision {
		reconcileTerraform(c)
	}

	if err := configureNodeDialers(c); err != nil {
		return err
	}

	if err := cluster.CopyFiles(); err != nil {
		return err
	}

	if err := cluster.UpgradeControlPlane(); err != nil {
		return err
	}

	if err := cluster.UpgradeWorkers(); err != nil {
		return err
	}
	return nil
}

// UpgradeControlPlane runs kubeadm upgrade apply on control plane nodes
func (c *Cluster) UpgradeControlPlane() error {
	log.Infof("[upgrade] running the kubeadm upgrade command")

	if len(c.ControlPlane.Nodes) >= 1 {
		initNode := c.ControlPlane.Nodes[0]

		if err := initNode.Connect(); err != nil {
			log.Fatalf("[upgrade] Failed to ssh into to [%s]. %s\n", initNode.Address, err)
		}
		defer initNode.Dialer.Client.Close()

		clusterConfig, initConfig := c.Cluster.ClusterConfiguration, c.Cluster.InitConfiguration
		kubeadmInitYaml, err := kubeadm.GetInitControlPlaneTmpl(clusterConfig, initConfig, c.ControlPlane.Nodes, initNode)
		if err != nil {
			log.Errorf("[upgrade] failed to render template. %s", err)
			log.Fatalf("[upgrade] kubeadm init config creation failed. %s", err)
		}
		log.Debugf("[upgrade] cluster configuration as yaml:\n %s", kubeadmInitYaml)

		log.Infof("[upgrade] writing kubeconfig init configfile")
		err = initNode.CopyFile(kubeadmInitYaml, 0666, kubeAdmInitConf)
		if err != nil {
			log.Fatalf("[upgrade] Failed to copy kubeadm config to [%s] %s", initNode.Address, err)
		}

		// ensure the control plane is running on the control plane node
		if !kubeadm.IsControlPlaneRunning(&initNode) {
			log.Errorf("[upgrade] [%s] control plane is not running on this node. failing.", initNode.Address)
			return err
		}

		// run the kubeadm upgrade command
		err = kubeadm.UpgradeApply(&initNode, true, kubeAdmInitConf, c.ClusterConfiguration.KubernetesVersion, false)
		if err != nil {
			log.Errorf("[upgrade] kubeadm upgrade command failed. %s", err)
			return err
		}

		// setup the cacerthash
		kubeadm.GetAdminKubeConf(&initNode, localKubeConfigConf)
		log.Infof("[cluster/upgrade] getting cacerthash pins for kubeadm join config")
		cacertHash, err := kubeadm.GetCACertHash(localKubeConfigConf)
		if err != nil {
			log.Errorf("[cluster/create] get ca cert hashes failed")
		}

		// upgrade the control plane nodes
		if len(c.ControlPlane.Nodes) > 1 {
			for _, node := range c.ControlPlane.Nodes[1:] {
				log.Infof("[upgrade] upgrading control plane node [%s] ", node.Address)
				if err := node.Connect(); err != nil {
					log.Fatalf("[upgrade] Failed to ssh into to [%s]. %s", node.Address, err)
				}
				defer node.Dialer.Client.Close()
				// get the join yaml config
				kubeadmJoinYaml, err := kubeadm.GetJoinControlPlaneYaml(clusterConfig, initConfig, node, cacertHash)
				if err != nil {
					log.Fatalf("[cluster/create] exec failed on [%s] %s", node.Address, err)
				}
				err = node.CopyFile(kubeadmJoinYaml, 0666, kubeAdmJoinConf)
				if err != nil {
					log.Fatalf("[upgrade] Failed to copy kubeadm config to [%s] %s", initNode.Address, err)
				}
				err = kubeadm.UpgradeApply(&node, false, kubeAdmJoinConf, c.ClusterConfiguration.KubernetesVersion, false)
				if err != nil {
					log.Fatalf("[upgrade] [%s] upgrade failed. %s", node.Address, err)
				}
			}
		}
	}
	log.Infof("[cluster/upgrade] Control plane upgrade finished.")
	return nil
}

// UpgradeWorkers upgrades the kubelets
func (c *Cluster) UpgradeWorkers() error {
	log.Infof("[cluster/upgrade] upgrading worker nodes")

	clusterConfig, initConfig := c.Cluster.ClusterConfiguration, c.Cluster.InitConfiguration
	cacertHash, err := kubeadm.GetCACertHash(localKubeConfigConf)
	if err != nil {
		log.Errorf("[cluster/upgrade] failed to get join worker yaml")
	}
	nodes := c.Worker.Nodes
	for _, node := range nodes {
		kubeadmJoinYaml, err := kubeadm.GetJoinWorkerYaml(clusterConfig, initConfig, node, cacertHash)
		if err := node.Connect(); err != nil {
			log.Fatalf("[cluster/upgrade] failed to ssh into to [%s]. %s", node.Address, err)
		}
		defer node.Dialer.Client.Close()
		err = node.CopyFile(kubeadmJoinYaml, 0666, kubeAdmJoinConf)
		if err != nil {
			log.Fatalf("[upgrade] Failed to copy kubeadm config to [%s] %s", node.Address, err)
		}

		err = kubeadm.UpgradeNode(&node, false, kubeAdmJoinConf, c.ClusterConfiguration.KubernetesVersion)
		if err != nil {
			log.Fatalf("[upgrade] [%s] upgrade failed. %s", node.Address, err)
		}
	}
	log.Infof("[cluster/upgrade] upgrade kubelets finished.")
	return nil
}
