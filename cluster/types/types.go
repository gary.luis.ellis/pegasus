package types

import (
	"github.com/garyellis/pegasus/dialer"
	kubeadmtypes "github.com/garyellis/pegasus/kubeadm/types/v1beta1"
)

type Cluster struct {
	KubernetesVersion    string
	ImageRepository      string
	ClusterName          string
	ControlPlaneEndpoint string
	ControlPlaneDNS      string
	BootstrapToken       string
	CertSANs             []string
	Networking
	ClusterConfiguration kubeadmtypes.ClusterConfiguration `mapstructure:"clusterConfiguration,omitempty"`
	InitConfiguration    kubeadmtypes.InitConfiguration    `mapstructure:"initConfiguration,omitempty"`
}

type Networking struct {
	PodSubnet string
}

type ControlPlane struct {
	Nodes []dialer.Node
}

type Worker struct {
	Nodes []dialer.Node
}

type Etcd struct {
	Nodes []dialer.Node
}
