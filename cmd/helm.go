package cmd

import (
	"github.com/garyellis/pegasus/config"
	"github.com/garyellis/pegasus/helm"
	"github.com/garyellis/pegasus/k8s"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

func HelmCmd() *cobra.Command {
	helmCmd := &cobra.Command{
		Use:   "helm [install]",
		Short: "helm",
		Long:  `deploy helm releases`,
	}

	helmCmd.PersistentFlags().StringVar(&configFile, "config", "config", "the config filename")
	helmCmd.AddCommand(helmInstallCmd())
	return helmCmd
}

func helmInstallCmd() *cobra.Command {
	helmInstallCmd := &cobra.Command{
		Use:   "install",
		Short: "installs the helm releases",
		Long:  `This command deploys the helm releases list defined in config.yaml`,
		RunE:  helmInstall,
	}

	return helmInstallCmd
}

func helmInstall(cmd *cobra.Command, args []string) error {
	cfg := config.GetConfig(configFile, configfilepath)
	k8sClient, err := k8s.NewClient("./conf/admin.conf")
	if err != nil {
		log.Fatalf("[cmd/helm] %s", err)
	}
	if err := helm.InstallHelmReleases(k8sClient, cfg.HelmReleases, cfg.HelmImage); err != nil {
		log.Fatalf("[cmd/helm] %s", err)
	}
	return nil
}
