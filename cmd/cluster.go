package cmd

import (
	"github.com/garyellis/pegasus/cluster"
	"github.com/garyellis/pegasus/config"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

var (
	configFile         string
	configfilepath     string
	skipInfraProvision bool
)

const clusterDesc = ``

// ClusterCmd runs kubeadm workflows on the cluster
func ClusterCmd() *cobra.Command {
	clusterCmd := &cobra.Command{
		Use:   "cluster",
		Short: "Manage kubernetes clusters",
		Long:  `The pegasus cli cluster command manages a kubernetes cluster using kubeadm`,
	}

	clusterCmd.PersistentFlags().StringVar(&configFile, "config", "config", "the config filename")
	clusterCmd.PersistentFlags().BoolVar(&skipInfraProvision, "skip-infra-provision", false, "skips the terraform infra provisioning phase")
	clusterCmd.AddCommand(createClusterCmd())
	clusterCmd.AddCommand(resetClusterCmd())
	clusterCmd.AddCommand(upgradeClusterCmd())

	return clusterCmd
}

func createClusterCmd() *cobra.Command {
	createClusterCmd := &cobra.Command{
		Use:   "create",
		Short: "bootstraps a kubernetes cluster",
		Long:  `create provisions infrastructure and initializes the kubernetes cluster.`,
		RunE:  createCluster,
	}

	return createClusterCmd
}

func resetClusterCmd() *cobra.Command {
	resetClusterCmd := &cobra.Command{
		Use:   "reset",
		Short: "reset the kubernetes cluster",
		Long:  `reset runs kubeadm reset on all Kubernetes cluster nodes.`,
		RunE:  resetCluster,
	}
	return resetClusterCmd
}

func upgradeClusterCmd() *cobra.Command {
	upgradeClusterCmd := &cobra.Command{
		Use:   "upgrade",
		Short: "Upgrade the kubernetes version and update the kubernetes cluster configuration.",
		Long:  `The upgrade command runs kubeadm upgrade commands on the cluster. It updates configuration on cluster nodes and when kubernetes version is updated in the cluster.yaml config, it will upgrade the kubernetes version.`,
		RunE:  upgradeCluster,
	}
	return upgradeClusterCmd
}

func createCluster(cmd *cobra.Command, args []string) error {
	log.Info("[cluster] Creating new cluster")
	cfg := config.GetConfig(configFile, configfilepath)
	err := cluster.Create(&cfg, skipInfraProvision)
	if err != nil {
		log.Fatalln(err)
	}

	return nil
}

func upgradeCluster(cmd *cobra.Command, args []string) error {
	log.Infof("[cluster] upgrading cluster")
	cfg := config.GetConfig(configFile, configfilepath)

	err := cluster.Upgrade(&cfg, skipInfraProvision)
	if err != nil {
		log.Fatalln(err)
	}

	return nil
}

func resetCluster(cmd *cobra.Command, args []string) error {
	log.Info("[cluster] Resetting cluster")
	cfg := config.GetConfig(configFile, configfilepath)

	err := cluster.Reset(&cfg, skipInfraProvision)
	if err != nil {
		log.Fatalln(err)
	}
	return nil
}
