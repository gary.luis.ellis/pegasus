package cmd

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"os"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
	"github.com/garyellis/pegasus/config"
	"github.com/garyellis/pegasus/util/shellutils"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"golang.org/x/net/context"
)

var (
	imagesConfigFile   string
	imagesPullAndSave  bool
	imagesPushFromFile bool
)

func ImagesCmd() *cobra.Command {
	imagesCmd := &cobra.Command{
		Use:   "images",
		Short: "Manages kubernetes docker images",
	}

	imagesCmd.PersistentFlags().StringVar(&imagesConfigFile, "config", "images", "the images config filename")
	imagesCmd.AddCommand(pullImagesCmd())
	imagesCmd.AddCommand(pushImagesCmd())
	return imagesCmd
}

func pullImagesCmd() *cobra.Command {
	pullImagesCmd := &cobra.Command{
		Use:   "pull",
		Short: "pulls kubernetes container images.",
		RunE:  pullImages,
	}

	pullImagesCmd.Flags().BoolVar(&imagesPullAndSave, "save", false, "save docker images to a tarball")
	return pullImagesCmd
}

func pushImagesCmd() *cobra.Command {
	pushImagesCmd := &cobra.Command{
		Use:   "push",
		Short: "push kubernetes container images to a private registry",
		RunE:  pushImages,
	}

	pushImagesCmd.Flags().BoolVar(&imagesPushFromFile, "file", false, "push docker images [WARNING] - this is not yet implemented")
	return pushImagesCmd
}

// pullImages  will move into a separate package
func pullImages(cmd *cobra.Command, args []string) error {
	log.Infof("[images] pre-pulling and loading images from config")
	imagesCfg := config.GetImagesConfig(imagesConfigFile, configfilepath)
	ctx := context.Background()
	cli, err := client.NewEnvClient()
	if err != nil {
		return err
	}

	var imagesMapping []ImageMapping
	for _, registry := range imagesCfg.Registries {
		imagesMapping = append(imagesMapping, collectImages(registry)...)
	}
	for _, i := range imagesMapping {
		log.Infof("[images] pulling %s", i.Src)
		out, err := cli.ImagePull(ctx, i.Src, types.ImagePullOptions{})
		if err != nil {
			return err
		}
		defer out.Close()
		shellutils.ReadStdOutErrPipe(out)

	}

	// create the tarball
	if imagesPullAndSave {
		images := getSrcImages(imagesMapping)

		imagesTarball, err := os.Create("images.tar")
		if err != nil {
			return err
		}
		defer imagesTarball.Close()

		// Save the images as a tarball
		out, err := cli.ImageSave(ctx, images)
		if err != nil {
			return err
		}
		log.Infof("[images] saving images as tarball images.tar")
		if _, err := io.Copy(imagesTarball, out); err != nil {
			return err
		}
	}
	return nil
}

func pushImages(cmd *cobra.Command, args []string) error {
	log.Infof("[images] pre-pulling and loading images from config")
	imagesCfg := config.GetImagesConfig(imagesConfigFile, configfilepath)
	ctx := context.Background()
	cli, err := client.NewEnvClient()
	if err != nil {
		return err
	}

	// get registry credentials from the environment
	pushOpts, err := getRegistryPushOpts()

	var imagesMapping []ImageMapping
	for _, registry := range imagesCfg.Registries {
		imagesMapping = append(imagesMapping, collectImages(registry)...)
	}

	for _, i := range imagesMapping {
		log.Infof("[images] pushing %s to %s", i.Src, i.Dst)

		if err := pushImage(ctx, cli, pushOpts, i); err != nil {
			return err
		}
	}
	return nil

}
func pushImage(ctx context.Context, cli *client.Client, r types.ImagePushOptions, image ImageMapping) error {
	log.Infof("[images] uploading image %s", image.Dst)

	if err := cli.ImageTag(ctx, image.Src, image.Dst); err != nil {
		return err
	}
	out, err := cli.ImagePush(ctx, image.Dst, r)
	if err != nil {
		return err
	}
	defer out.Close()
	shellutils.ReadStdOutErrPipe(out)
	return nil
}

func getRegistryPushOpts() (types.ImagePushOptions, error) {
	pushOpts := types.ImagePushOptions{All: true, RegistryAuth: "requiredheader"}

	registryUser, registryUserSet := os.LookupEnv("REGISTRY_USERNAME")
	registryPassword, registryPasswordSet := os.LookupEnv("REGISTRY_PASSWORD")

	if !registryUserSet || !registryPasswordSet {
		log.Warnln("[images] registry credentials environment is not set. Skipping auth config.")
	} else {
		log.Infoln("[images] setting registry auth config username and password")
		authConfig := types.AuthConfig{
			Username: registryUser,
			Password: registryPassword,
		}

		encodedAuthConfig, err := json.Marshal(authConfig)
		if err != nil {
			return pushOpts, err
		}
		authStr := base64.URLEncoding.EncodeToString(encodedAuthConfig)
		pushOpts.RegistryAuth = authStr
	}
	return pushOpts, nil
}

type ImageMapping struct {
	Src string
	Dst string
}

func collectImages(r config.Registry) []ImageMapping {
	var imageMapping []ImageMapping
	for _, image := range r.Images {
		for _, tag := range image.Tags {
			i := ImageMapping{
				Src: fmt.Sprintf("%s/%s:%s", r.SrcRegistryName, image.SrcRepository, tag),
				Dst: fmt.Sprintf("%s/%s:%s", r.DstRegistryName, image.DstRepository, tag),
			}
			imageMapping = append(imageMapping, i)
		}
	}
	return imageMapping
}

func getSrcImages(images []ImageMapping) []string {
	var srcImages []string
	for _, i := range images {
		srcImages = append(srcImages, i.Src)
	}
	return srcImages
}

func getDstImages(images []ImageMapping) []string {
	var dstImages []string
	for _, i := range images {
		dstImages = append(dstImages, i.Dst)
	}
	return dstImages
}
