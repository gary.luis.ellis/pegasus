package cmd

import (
	"errors"
	"fmt"

	"github.com/spf13/cobra"

	"github.com/garyellis/pegasus/config"
)

const (
	awsStackedEtcd     = "aws-stacked-etcd"
	awsExternalEtcd    = "aws-external-etcd"
	vmwareStackedEtcd  = "vmware-stacked-etcd"
	vmwareExternalEtcd = "vmware-external-etcd"
	custom             = "custom"
)

var (
	clusterTopology     string
	awsCloudProvider    bool
	vmwareCloudProvider bool
)

// ConfigCmd prints config scaffolding. It is a work in progress as a config file helper.
// It will mature into a project scaffolding generator to simplify git repo setup for users.
func ConfigCmd() *cobra.Command {
	configCmd := &cobra.Command{
		Use:   "config",
		Short: "print config yaml scaffoldings",
		Long:  `cluster config yaml scaffolding for multiple deployment topologies`,
	}
	configCmd.AddCommand(configPrintCmd())

	return configCmd
}

func configPrintCmd() *cobra.Command {
	configPrintCmd := &cobra.Command{
		Use:   "print",
		Short: "print cluster yaml configurations",
		RunE:  configPrint,
	}
	configPrintCmd.Flags().StringVar(&clusterTopology, "cluster-topology", "", "valid values are [aws-stacked-etcd, vmware-stacked-etcd, custom]")
	configPrintCmd.Flags().BoolVar(&awsCloudProvider, "aws-cloud-provider", false, "add aws cloud provider config")
	configPrintCmd.Flags().BoolVar(&vmwareCloudProvider, "vmware-cloud-provider", false, "add vmware cloud provider config")

	return configPrintCmd
}

func configPrint(cmd *cobra.Command, args []string) error {
	switch clusterTopology {
	case awsStackedEtcd:
		yamlConfig, _ := config.GetAWSStackedEtcdConfigTmpl()
		fmt.Printf(string(yamlConfig))
	case vmwareStackedEtcd:
		yamlConfig, _ := config.GetVMWareStackedEtcdConfigTmpl()
		fmt.Printf(string(yamlConfig))
	case custom:
		yamlConfig, _ := config.GetCustomConfigTmpl()
		fmt.Printf(string(yamlConfig))
	default:
		return errors.New("A valid cluster topology is required.")
	}
	return nil
}
