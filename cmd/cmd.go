package cmd

import (
	"os"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

// NewRootCmd wires up pegasus cli commands
func NewRootCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:     "pegasus",
		Short:   "pegasus cli",
		Version: Version,
	}

	cmd.AddCommand(VersionCmd())
	cmd.AddCommand(InfraCmd())
	cmd.AddCommand(ClusterCmd())
	cmd.AddCommand(BootstrapTokenCmd())
	cmd.AddCommand(ConfigCmd())
	cmd.AddCommand(ImagesCmd())
	cmd.AddCommand(AddOnsCmd())
	cmd.AddCommand(HelmCmd())
	return cmd
}

func init() {
	loglevelenv, ok := os.LookupEnv("LOG_LEVEL")
	if !ok {
		loglevelenv = "info"
	}
	loglevel, err := logrus.ParseLevel(loglevelenv)
	if err != nil {
		loglevel = logrus.InfoLevel
	}
	logrus.SetLevel(loglevel)
}
