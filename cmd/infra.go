package cmd

import (
	"github.com/garyellis/pegasus/config"
	"github.com/garyellis/pegasus/terraform"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

// CreateInfraCmd creates the infra from the yaml config
func InfraCmd() *cobra.Command {
	infraCmd := &cobra.Command{
		Use:   "infra [command]",
		Short: "infra",
		Long:  `create the cluster infrastructure`,
	}

	infraCmd.PersistentFlags().StringVar(&configFile, "config", "config", "the config filename")
	infraCmd.AddCommand(planInfraCmd())
	infraCmd.AddCommand(createInfraCmd())
	infraCmd.AddCommand(destroyInfraCmd())
	infraCmd.AddCommand(outputInfraCmd())

	return infraCmd
}

func planInfraCmd() *cobra.Command {
	planInfraCmd := &cobra.Command{
		Use:   "plan",
		Short: "show the expected infrastructure plan",
		Long:  `This command runs terraform plan on the cluster infrastructure`,
		RunE:  planInfra,
	}

	return planInfraCmd
}

func createInfraCmd() *cobra.Command {
	createInfraCmd := &cobra.Command{
		Use:   "create",
		Short: "create the infrastructure",
		Long:  `This command creates the cluster infrastructure with terraform`,
		RunE:  createInfra,
	}

	return createInfraCmd
}

func destroyInfraCmd() *cobra.Command {
	destroyInfraCmd := &cobra.Command{
		Use:   "destroy",
		Short: "destroy the infrastructure",
		Long:  `This command destroys the cluster infrastructure with terraform`,
		RunE:  destroyInfra,
	}

	return destroyInfraCmd
}

func outputInfraCmd() *cobra.Command {
	outputInfraCmd := &cobra.Command{
		Use:   "output",
		Short: "terraform output",
		Long:  `This command prints the terraform outputs`,
		RunE:  outputInfra,
	}

	return outputInfraCmd
}

// planInfra shows the cluster infrastructure plan
func planInfra(cmd *cobra.Command, args []string) error {
	cfg := config.GetConfig(configFile, configfilepath)
	err := terraform.Plan(cfg.Terraform.Modules)
	if err != nil {
		log.Errorf("[infra] Plan failed. %s", err)
		return err
	}
	return nil
}

// createInfra creates the cluster infrastructure but does not bootstrap kubernetes cluster.
func createInfra(cmd *cobra.Command, args []string) error {
	cfg := config.GetConfig(configFile, configfilepath)

	_, _, _, err := terraform.Create(cfg.Terraform.Modules)
	if err != nil {
		log.Errorf("[infra] Failed. %s", err)
		return err
	}
	return nil
}

// destroyInfra destroys the cluster infrastructure
func destroyInfra(cmd *cobra.Command, args []string) error {
	cfg := config.GetConfig(configFile, configfilepath)
	err := terraform.Destroy(cfg.Terraform.Modules)
	if err != nil {
		log.Errorf("[infra] Failed. %s", err)
		return err
	}
	return nil
}

// WIP terraform output json or yaml
func outputInfra(cmd *cobra.Command, args []string) error {
	cfg := config.GetConfig(configFile, configfilepath)
	if err := terraform.Output(cfg.Terraform.Modules); err != nil {
		log.Errorf("[infra] %s", err)
	}
	return nil
}
