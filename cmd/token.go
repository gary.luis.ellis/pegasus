package cmd

import (
	"fmt"
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	bootstraputil "k8s.io/cluster-bootstrap/token/util"
	clustercerts "k8s.io/kubernetes/cmd/kubeadm/app/phases/certs"
)

var updateConfig bool

// BootstrapTokenCmd generates a bootstrap token and optionally sets it in the config file.
func BootstrapTokenCmd() *cobra.Command {
	generateBootstrapTokenCmd := &cobra.Command{
		Use:   "token",
		Short: "token",
		Long:  `generate a bootstrap token`,
		RunE:  GenerateBootstrapToken,
	}

	generateBootstrapTokenCmd.PersistentFlags().BoolVar(&updateConfig, "updateconfig", false, "update the config file with the bootstrap token")
	generateBootstrapTokenCmd.Flags().StringVar(&configFile, "config", "config", "the config filename")
	// add ttl and description

	return generateBootstrapTokenCmd
}

// GenerateBootstrapToken writes the bootstrap token
func GenerateBootstrapToken(cmd *cobra.Command, args []string) error {
	log.Infoln("generating bootstrap token")
	token, err := bootstraputil.GenerateBootstrapToken()

	if err != nil {
		return err
	}

	log.Infoln("generating new service account signing key")
	saKey, err := clustercerts.NewServiceAccountSigningKey()
	if err != nil {
		return err
	}
	log.Infoln("key:", saKey)

	fmt.Fprintln(os.Stdout, token)
	return nil
}

// create certificates bundle with client-go cert util
// https://godoc.org/k8s.io/client-go/util/cert
