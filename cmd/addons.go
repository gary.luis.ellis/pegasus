package cmd

import (
	"github.com/garyellis/pegasus/addons"
	"github.com/garyellis/pegasus/config"
	"github.com/garyellis/pegasus/k8s"
	"github.com/spf13/cobra"
)

func AddOnsCmd() *cobra.Command {
	addOnsCmd := &cobra.Command{
		Use:   "addons [apply]",
		Short: "addons",
		Long:  `deploy addons`,
	}

	addOnsCmd.PersistentFlags().StringVar(&configFile, "config", "config", "the config filename")
	addOnsCmd.AddCommand(applyAddOnsCmd())
	return addOnsCmd
}

func applyAddOnsCmd() *cobra.Command {
	applyAddOnsCmd := &cobra.Command{
		Use:   "apply",
		Short: "applies the kubernetes addons",
		Long:  `This command applies the addons defined in config.yaml`,
		RunE:  applyAddOns,
	}

	return applyAddOnsCmd
}

func applyAddOns(cmd *cobra.Command, args []string) error {
	cfg := config.GetConfig(configFile, configfilepath)
	_, err := k8s.NewClient("./conf/admin.conf")
	if err != nil {
		return err
	}
	if err := addons.DoAddOns("./conf/admin.conf", cfg.AddOnsImage, cfg.AddOns); err != nil {
		return err
	}
	return nil
}
