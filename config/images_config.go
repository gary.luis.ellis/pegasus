package config

import (
	"fmt"
	"log"
	"path/filepath"

	"github.com/spf13/viper"
)

type ImagesConfig struct {
	Registries []Registry `mapstructure:"registries,omitempty"`
}

type Registry struct {
	SrcRegistryName string  `mapstructure:"src_registry_name" yaml:"src_registry_name"`
	DstRegistryName string  `mapstructure:"dst_registry_name" yaml:"dst_registry_name"`
	Images          []Image `mapstructure:"images,omitempty" yaml:"images,omitempty"`
}
type Image struct {
	SrcRepository string   `mapstructure:"src_repository" yaml:"src_repository"`
	DstRepository string   `mapstructure:"dst_repository" yaml:"dst_repository"`
	Tags          []string `mapstructure:"tags,omitempty" yaml:"tags,omitempty"`
}

// ImageMapping will be removed
type ImageMapping struct {
	Src string
	Dst string
}

// CollectImages method will be removed
func (r *Registry) CollectImages() []ImageMapping {
	var imageMapping []ImageMapping
	for _, image := range r.Images {
		for _, tag := range image.Tags {
			i := ImageMapping{
				Src: fmt.Sprintf("%s/%s:%s", r.SrcRegistryName, image.SrcRepository, tag),
				Dst: fmt.Sprintf("%s/%s:%s", r.DstRegistryName, image.DstRepository, tag),
			}
			imageMapping = append(imageMapping, i)
		}
	}
	return imageMapping
}

// GetConfig unmarshals the config file to config struct
func GetImagesConfig(filename, path string) ImagesConfig {
	var ext = filepath.Ext(filename)
	var name = filename[0 : len(filename)-len(ext)]

	viper.SetConfigType("yaml")
	viper.SetConfigName(name)
	viper.AddConfigPath(".")
	var imagesConfig ImagesConfig

	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("[config] error reading config file, %s", err)
	}

	err := viper.Unmarshal(&imagesConfig)
	if err != nil {
		log.Fatalf("[config] unable to decode config into struct, %v", err)
	}

	return imagesConfig
}
