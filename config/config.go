package config

import (
	"path/filepath"

	"github.com/garyellis/pegasus/addons"
	"github.com/garyellis/pegasus/certs"
	clustertypes "github.com/garyellis/pegasus/cluster/types"
	"github.com/garyellis/pegasus/dialer"
	"github.com/garyellis/pegasus/helm"
	terraformtypes "github.com/garyellis/pegasus/terraform/types"

	"github.com/a8m/envsubst"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gopkg.in/yaml.v2"
)

type Config struct {
	*terraformtypes.Terraform
	*certs.Certs
	*clustertypes.Cluster
	SSHConfig `mapstructure:"sshconfig,omitempty"`
	clustertypes.ControlPlane
	clustertypes.Worker
	clustertypes.Etcd
	Files        []File
	PreTasks     []dialer.Task
	AddOns       []addons.AddOn
	AddOnsImage  string             `mapstructure:"addonsimage,omitempty"`
	HelmReleases []helm.HelmRelease `mapstructure:"helmreleases,omitempty"`
	HelmImage    string             `mapstructure:"helmimage,omitempty"`
}

// File represents a file to copy to the remote nodes.
// This type should move into cluster package because it is implemented there.
type File struct {
	Name       string   `mapstructure:"name,omitempty"`
	Content    string   `mapstructure:"content,omitempty"`
	Permission int      `mapstructure:"permission,omitempty"`
	Dest       string   `mapstructure:"dest,omitempty"`
	NodeLabels []string `mapstructure:"nodelabels,omitempty"`
}

// SSHConfig should probably move to the nodes aka "dialer" package because it is implemented there.
type SSHConfig struct {
	User       string `mapstructure:"user,omitempty"`
	PrivateKey string `mapstructure:"key,omitempty"`
	Password   string `mapstructure:"password,omitempty"`
}

// GetConfig unmarshals the config file to config struct
func GetConfig(filename, path string) Config {
	var ext = filepath.Ext(filename)
	var name = filename[0 : len(filename)-len(ext)]

	viper.SetConfigType("yaml")
	viper.SetConfigName(name)
	viper.AddConfigPath(".")
	config := newConfig()

	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("[config] error reading config file, %s", err)
	}

	err := viper.Unmarshal(&config)
	if err != nil {
		log.Fatalf("[config] unable to decode config into struct, %v", err)
	}

	if err := config.EnvSubst(); err != nil {
		log.Fatalf("[config] quitting.")
	}

	return config
}

func (c *Config) AddEtcdNodes(nodes []dialer.Node) {
	c.Etcd.Nodes = append(c.Etcd.Nodes, nodes...)
}

func (c *Config) AddControlPlaneNodes(nodes []dialer.Node) {
	c.ControlPlane.Nodes = append(c.ControlPlane.Nodes, nodes...)
}

func (c *Config) AddWorkerNodes(nodes []dialer.Node) {
	c.Worker.Nodes = append(c.Worker.Nodes, nodes...)
}

func (c *Config) SetEtcdNodes(nodes []dialer.Node) {
	c.Etcd.Nodes = nodes
}

func (c *Config) SetControlPlaneNodes(nodes []dialer.Node) {
	c.ControlPlane.Nodes = nodes
}

func (c *Config) SetWorkerNodes(nodes []dialer.Node) {
	c.Worker.Nodes = nodes

}

func (c *Config) EnvSubst() error {
	clusterRaw, err := envSubst(c.Cluster)
	if err != nil {
		log.Errorf("[config] env substitution failed for cluster. %s", err)
	}
	if err := yaml.Unmarshal(clusterRaw, &c.Cluster); err != nil {
		log.Errorf("[config] env substitution unmarshal to cluster config failed. %s", err)
		return err
	}

	sshRaw, err := envSubst(c.SSHConfig)
	if err != nil {
		log.Errorf("[config] env substitution failed for ssh config. %s", err)
	}
	if err := yaml.Unmarshal(sshRaw, &c.SSHConfig); err != nil {
		log.Errorf("[config] env substitution unmarshal to cluster config failed. %s", err)
		return err
	}

	certsRaw, err := envSubst(c.Certs)
	if err != nil {
		log.Errorf("[config] env substitution failed for certs config. %s", err)
	}
	if err := yaml.Unmarshal(certsRaw, &c.Certs); err != nil {
		log.Errorf("[config] env substitution unmarshal to cluster config failed. %s", err)
		return err
	}

	files, err := envSubst(c.Files)
	if err != nil {
		log.Errorf("[config] env substitution failed for files config. %s", err)
	}
	if err := yaml.Unmarshal(files, &c.Files); err != nil {
		log.Errorf("[config] env substitution unmarshal for files config failed. %s", err)
	}

	addonsList, err := envSubst(c.AddOns)
	if err != nil {
		log.Errorf("[config] env substitution failed for addons config. %s", err)
	}
	if err := yaml.Unmarshal(addonsList, &c.AddOns); err != nil {
		log.Errorf("[config] env substitution failed to unmarshal addons config. %s", err)
	}

	helmreleases, err := envSubst(c.HelmReleases)
	if err != nil {
		log.Errorf("[config] env substitution failed on helmreleases config. %s", err)
	}
	if err := yaml.Unmarshal(helmreleases, &c.HelmReleases); err != nil {
		log.Errorf("[config] env substitution yaml unmarshal failed on helmreleases config. %s", err)
	}

	// we limit users the option to apply env substitution on terraform because it already provides a system to manage variables and environment.
	// terraform blocks variable management is not supported by terraform. env substitution on these blocks is useful so we support it in our config.
	for i, m := range c.Terraform.Modules {
		if m.TerraformBlock == nil {
			continue
		} else {
			tfBlockRaw, err := envSubst(c.Terraform.Modules[i].TerraformBlock)
			if err != nil {
				log.Errorf("[config] env substitution on terraform block failed. %s", err)
				return err
			}
			if err := yaml.Unmarshal(tfBlockRaw, &c.Terraform.Modules[i].TerraformBlock); err != nil {
				log.Errorf("[config] env substitution unmarshal on terraform block failed. %s", err)
				return err
			}
		}
	}

	return nil
}

func envSubst(i interface{}) ([]byte, error) {
	b, err := yaml.Marshal(i)
	if err != nil {
		log.Errorf("[config] env substitution failed. %s", err)
		return nil, err
	}

	rendered, err := envsubst.Bytes(b)
	if err != nil {
		log.Errorf("[config] env substitution failed. %s", err)
		return nil, err
	}
	return rendered, nil
}

// newConfig sets config defaults
func newConfig() Config {
	return Config{
		AddOnsImage: "gcr.io/google-containers/hyperkube:v1.14.2",
		HelmImage:   "quay.io/garyellis/helm:v2.14.3",
	}
}
