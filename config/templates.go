package config

import (
	"bytes"
	"text/template"
)

const awsStackedEtcdConfigEnvTmpl = `# terraform config
export TF_VAR_name="<cluster-name>"
export TF_VAR_cluster_name="$TF_VAR_name"
export TF_VAR_dns_zone="<dns-zone>"
export TF_VAR_dns_zone_id="<dns-zone-id>"
export TF_VAR_vpc_id="<vpc-id>"
export TF_VAR_private_subnets='["<subnet-id-0>", "<subnet-id-1>", "<subnet-id-2>"]'
export TF_VAR_iam_instance_profile="<iam-instance-profile"
export TF_VAR_key_name="<key-name>"

# sets the number of control plane nodes
export TF_VAR_count_control_plane_nodes="3"

# sets the number of worker nodes
export TF_VAR_count_worker_nodes="3"


# cluster config
export CLUSTER_NAME="$TF_VAR_cluster_name"
export CLUSTER_PORT="6443"
export CLUSTER_TLS_CIPHER_SUITES="TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384"
# bootstrap token environment variable is protected by the user. It should be moved from this file.
export CLUSTER_BOOTSTRAP_TOKEN="<bootstrap-token>"

# ssh config
export SSH_USER=centos
# uncomment ssh password when password auth is used
# export SSH_PASSWORD='<ssh-password>"
export SSH_KEY="<ssh-key-path>"
`

const awsStackedEtcdConfigTmpl = `---
terraform:
  ## modules is a list of terraform module instances.
  modules:
    # a module terraform json expressed as yaml
    # provider, data, resource, module, variable, output and terraform blocks are supported
    # see the terraform docs for terraform json usage details
    # terraform v0.12 - https://www.terraform.io/docs/configuration/syntax-json.html
    # terraform v0.11 - https://www.terraform.io/docs/configuration-0-11/index.html
    - variable:
        name:
          type: string
        cluster_name:
          type: string
        ami_name:
          type: string
          default: "CentOS Linux 7 x86_64 HVM EBS ENA 1805_01-b7ee8a69-ee97-4a49-9e68-afaee216db2e-ami-77ec9308.4"
        count_worker_nodes:
          type: string
        worker_node_instance_type:
          type: string
          default: "t2.large"
        dns_zone: 
          type: string
        dns_zone_id:
          type: string
        vpc_id:
          type: string
        private_subnets:
           type: list
        security_group_attachments:
          type: list
        iam_instance_profile:
          type: string
        key_name:
          type: string
      # module contains a map of terraform modules
      # terraform module parameters can be set explicitly or as variables defined in the variable map
      module:
        control_plane:
          # any module source can be used here; local relative module sources must start with "./"
          source: "git::ssh://git@gitlab.com/gary.luis.ellis/k8s-init.git//deploy/terraform/modules/aws-stacked-etcd-existing-vpc?ref=master"
          ami_name: "${var.ami_name}"
          cluster_name: "${var.cluster_name}"
          name: "${var.name}"
          key_name: "${var.key_name}"
          iam_instance_profile: "${var.iam_instance_profile}"
          count_control_plane_nodes: "${var.count_control_plane_nodes}"
          count_worker_nodes: "${var.count_worker_nodes}"
          security_group_attachments: ["${var.security_group_attachments}"]
          private_subnets: ["${var.private_subnets}"]
          vpc_id: "${var.vpc_id}"
          dns_zone: "${var.dns_zone}"
          dns_zone_id: "${var.dns_zone_id}"
          worker_instance_type: "t2.large"
          tags:
            environment_stage: prod
            environment_name: pegasus-k8s
            owner: me
      ## outputs wire module output mappings
      output:
        # control_plane_nodes output is a required list of node mappings.
        control_plane_nodes:
          value: "${module.control_plane.control_plane_nodes}"
          # control_plane_nodes output is a required list of node mappings.
        worker_nodes:
          value: "${module.control_plane.worker_nodes}"

      ## terraform block sets up version constraint and back end config
      ## environment variable substitution IS supported here
      terraform:
        backend:
          s3:
            region: ${AWS_REGION}
            bucket: ${PEGASUS_S3_BUCKET}
            key: ${PEGASUS_S3_BUCKET_KEY}

# custom cluster ca certs is supported.
# omit cert mappings to use kubeadm self signed certificates.
certs:
  # all ca certs must be on the local filesystem.
  #kubecacert: conf/certs/kube.ews.works-ca.pem
  #kubecakey: conf/certs/kube.ews.works-ca-key.pem
  #etcdcacert: conf/certs/etcd/etcd.ews.works-ca.pem
  #etcdcakey: conf/certs/etcd/etcd.ews.works-ca-key.pem
  #frontproxycacert: conf/certs/front-proxy.ews.works-ca.pem
  #frontproxycakey: conf/certs/front-proxy.ews.works-ca-key.pem

# cluster contains kubeadm clusterConfiguration and initConfiguration
# For configuration details see the kubeadm version api godocs
# kubernetes v1.14 - https://godoc.org/k8s.io/kubernetes/cmd/kubeadm/app/apis/kubeadm/v1beta1

cluster:
  clusterConfiguration:
    kubernetesVersion: "v1.14.2"
    clusterName: "${CLUSTER_NAME}"
    controlPlaneEndpoint: "${CLUSTER_NAME}.${TF_VAR_dns_zone}:${CLUSTER_PORT}"
    networking:
      podSubnet: 10.244.0.0/16
    dns:
      type: CoreDNS
    etcd:
      local:
        imageRepository: "quay.io/coreos"
        imageTag: "v3.3.13"
    apiServer:
      certSANs:
        - ${CLUSTER_NAME}.${TF_VAR_dns_zone}
      extraArgs:
        audit-log-path: /var/log/apiserver/audit.log
        audit-log-maxbackup: "10"
        audit-log-maxsize: "100"
        audit-log-maxage: "15"
        profiling: false
        service-account-lookup: "true"
        tls-cipher-suites: ${CLUSTER_TLS_CIPHER_SUITES}
    scheduler:
      extraArgs:
        bind-address: "0.0.0.0"
        profiling: "false"
      extraVolumes:
    controllerManager:
      extraArgs:
        profiling: "false"
        terminated-pod-gc-threshold: "10000"
    kubelet:
      extraArgs:
        #fail-swap-on: "false"

  initConfiguration:
    bootstrapTokens:
     - token: "${CLUSTER_BOOTSTRAP_TOKEN}"
       description: "The kubeadm bootstrap token"
       ttl: "0"
    nodeRegistration:
    localAPIEndpoint:
      bindPort: ${CLUSTER_PORT}

## ssh config is required to run commands on cluster nodes.
sshconfig:
  user: ${SSH_USER}
  key: /Users/gary/.ssh/id_rsa
  password: "${SSH_PASSWORD}"
# pretasks are an optional user defined set of tasks to run on on all cluster nodes prior to cluster creation.
# for users without OS configuration or AMI build configuration established.
pretasks:
  # by default, docker is installed by terraform multi-part cloud-init scripts
  # kubernetes install will be moved into pegasus-cli to control kubeadm upgrade workflows.
  - name: install kubernetes binaries - centos
    command: |
        base64 <<<"#!/bin/bash
        cat > /etc/yum.repos.d/kubernetes.repo <<EOF
        [kubernetes]
        name=Kubernetes
        baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
        enabled=1
        gpgcheck=1
        repo_gpgcheck=1
        gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
        exclude=kube*
        EOF
        yum install -y kubelet-1.14.2 kubeadm-1.14.2 kubectl-1.14.2 --disableexcludes=kubernetes
        # Set SELinux in permissive mode (effectively disabling it)
        setenforce 0
        sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config
        systemctl enable --now kubelet
        cat > /etc/sysctl.d/k8s.conf <<EOF
        net.bridge.bridge-nf-call-ip6tables = 1
        net.bridge.bridge-nf-call-iptables = 1
        EOF
        sysctl --system" | base64 -d | sudo bash

# opting out of terraform infrastructure provsiioning is supported for bare-metal deployments
# to skip terraform infra provisioning, static nodes configuration can be set here.
## etcd is an optional pre defined list of etcd nodes
etcd:
## controlplane is an optional pre defined list of control plane nodes
controlplane:

## worker is an optional pre defined list of worker nodes. 
worker:


# addons is a list of kubernetes manifests to deploy after cluster creation.
# paths may be http and local paths; inline yaml is not currently supported.
addons:
#- name: canal
#  path: ./addons/<my-addon>.yaml
#- name: example-http-addon
#  url: https://gist.githubusercontent.com/garyellis/ff3c1bc0f0bbac2a8c957f75b39ddd77/raw/76c0c5cc5af03e3db2b8cead59728a216c2ebdd9/aws-ebs-storageclass.yaml
`

const vmwareStackedEtcdConfigTmpl = `---`

const customTopologyTmpl = `---`

// GetAWSStackedEtcdConfigTmpl returns an aws stacked etcd compiled template config
func GetAWSStackedEtcdConfigTmpl() ([]byte, error) {
	yamlConfig := map[string]interface{}{}
	return CompileTemplateFromMap(awsStackedEtcdConfigTmpl, yamlConfig)
}

// GetVMWareStackedEtcdConfigTmpl returns a vmware stacked etcd compiled template config
func GetVMWareStackedEtcdConfigTmpl() ([]byte, error) {
	yamlConfig := map[string]interface{}{}
	return CompileTemplateFromMap(vmwareStackedEtcdConfigTmpl, yamlConfig)
}

// GetCustomConfigTmpl returns a custom compiled template config
func GetCustomConfigTmpl() ([]byte, error) {
	yamlConfig := map[string]interface{}{}
	return CompileTemplateFromMap(customTopologyTmpl, yamlConfig)
}

func CompileTemplateFromMap(tmplt string, input interface{}) ([]byte, error) {
	out := new(bytes.Buffer)
	t := template.Must(template.New("compiled_template").Parse(tmplt))
	if err := t.Execute(out, input); err != nil {
		return nil, err
	}
	return out.Bytes(), nil
}
