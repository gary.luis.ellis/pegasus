package main

import (
	"os"

	"github.com/garyellis/pegasus/cmd"
)

func main() {
	if err := cmd.NewRootCmd().Execute(); err != nil {
		os.Exit(1)
	}
}
