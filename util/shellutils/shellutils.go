package shellutils

import (
	"bufio"
	"io"
	"sync"

	log "github.com/sirupsen/logrus"
)

// ReadStdOutErrPipe logs stdout and stderr stream and returns contents
func ReadStdOutErrPipe(r io.ReadCloser) ([]byte, error) {
	var b []byte
	s := bufio.NewScanner(r)
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		for s.Scan() {
			log.Debugf("[util/shellutils] %s", s.Text())
			b = append(b, s.Bytes()...)
		}
		wg.Done()
	}()
	wg.Wait()
	return b, nil
}
