package fileutils

import (
	"log"
	"os"
)

// MakeDir creates a directory resursively.
func MakeDir(path string) error {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		mode := 0755
		err := os.MkdirAll(path, os.FileMode(mode))
		if err != nil {
			log.Fatalln(err)
		}
	}
	return nil
}
