.PHONY: help
	.DEFAULT_GOAL := help

help: ## show this message
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'



build: ## build the binary
	docker build -t pegasus/pegasus-cli .
	docker create -it --name pegasus-cli pegasus/pegasus-cli
	docker cp pegasus-cli:/release/ .
	docker save pegasus/pegasus-cli | gzip -c > pegasus-cli-image.tar.gz

release: ## release the binary and docker image
	echo 'release to somewhere'
	docker rm -f pegasus-cli

## 
## build workflow example
# any branch
#  - build
#  - release
# when master and tagged
#  - build injecting tag value as version
#  - release

## Notes
# creating tags in your own repository - https://community.atlassian.com/t5/Bitbucket-Pipelines-articles/Pushing-back-to-your-repository/ba-p/958407