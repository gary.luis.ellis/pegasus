## Changelog
## Notable changes in v0.2.4

* Added helm releases to config
* Added helm install releases subcommand
* Refactored addons into its own package
* Added addons apply subcommand
* Added environment substitution support to addons yaml

## Notable changes in v0.2.3

* Added infra plan command
* Added addons job deployer image override to config
* Added user defined node labels
* Added copy files workflow step
* Added kubelet extra args for control plane
* Fixed an issue with upgrade workflow preventing kubelet start opts from updating
* Log message tweaks

## Notable changes in v0.2.2

* Added terraform module sources support for relative paths
* Added aws-stacked-etcd cluster topology to config print command
* Fixed an cert pack/unpack issue affecting systems with a restrictive umask default

## Notable changes in v0.2.1

* Added environment variable substitution for certs, sshconfig, and cluster yaml config.
* Added environment variable substitution for terraform blocks in terraform modules yaml config.
* Fixed a bug preventing kubeadm from generating self signed certificates. 
* Fixed a bug preventing kubeadm from fetching certificates when local ./conf directory is not present.
* Fixed a bug that logged ssh password to stdout. it is now obfuscated.

## Notable changes in v0.2.0

* Added docker image pre-pull and load commands
* Added ssh password authentication
* Added yaml config print command.
* Refactored dialer package
* Added support to allow merge pre-defined nodes configuration to terraform configuration.
