package kubeadm

import (
	"fmt"

	"github.com/garyellis/pegasus/dialer"

	log "github.com/sirupsen/logrus"
)

// KubeAdmInit copies the kubeadm yaml config and initializes cluster
func KubeAdmInit(n *dialer.Node, joinYaml []byte, configPath string) error {
	log.Infof("[kubeadm] Running kubeadm init on [%s]", n.Address)
	commands := []dialer.Command{
		dialer.Command{
			Name:    "kubeadm-init",
			Command: KubeAdmInitCommand(configPath),
		},
	}

	// upload the yaml cfg
	if err := uploadYamlConfig(n, joinYaml, configPath); err != nil {
		return err
	}

	if err := doCommands(n, commands); err != nil {
		return err
	}

	log.Infof("[kubeadm] kubeadm init completed successfully on [%s]", n.Address)
	return nil
}

func KubeAdmInitCommand(configPath string) string {
	return fmt.Sprintf(`sudo bash -c "kubeadm init --config %s --skip-token-print --ignore-preflight-errors=all"`, configPath)
}

// KubeAdmJoin copies the kubeadm yaml config to the remote node and joins the node to the cluster
func KubeAdmJoin(n *dialer.Node, joinYaml []byte, configPath string) error {
	log.Infof("[kubeadm] Running kubeadm join on node [%s]", n.Address)
	commands := []dialer.Command{
		dialer.Command{
			Name:    "kubeadm-join",
			Command: KubeAdmJoinCommand(configPath),
		},
	}

	// upload the yaml cfg
	if err := uploadYamlConfig(n, joinYaml, configPath); err != nil {
		return err
	}

	// Join the node to the cluster
	if err := doCommands(n, commands); err != nil {
		return err
	}
	return nil
}

// KubeAdmJoinCommand returns the kubeadm join command
func KubeAdmJoinCommand(configPath string) string {
	return fmt.Sprintf(`sudo bash -c "kubeadm join --config %s --ignore-preflight-errors=all"`, configPath)
}

// KubeAdmReset resets the cluster node
func KubeAdmReset(n *dialer.Node) error {
	log.Infof("[kubeadm] Running kubeadm reset on [%s]", n.Address)
	commands := []dialer.Command{
		dialer.Command{
			Name:    "kubeadm-reset",
			Command: KubeAdmResetCommand(),
		},
		dialer.Command{
			Name:    "iptables flush",
			Command: `sudo bash -c "iptables -F && iptables -t nat -F && iptables -t mangle -F && iptables -X"`,
		},
	}

	if err := doCommands(n, commands); err != nil {
		return err
	}

	log.Infof("[kubeadm] kubeadm reset completed on [%s]", n.Address)
	return nil
}

// KubeAdmResetCommand runs kubeadm reset
func KubeAdmResetCommand() string {
	return fmt.Sprintf(`sudo bash -c "kubeadm reset -f"`)
}

// UpgradeApply runs kubeadm upgrade apply on control plane nodes
func UpgradeApply(n *dialer.Node, isInitNode bool, kubeadmYamlPath string, version string, dryRun bool) error {
	log.Infof("[kubeadm] Running kubeadm upgrade on [%s]", n.Address)
	commands := []dialer.Command{}
	var args string
	if dryRun {
		args = "--dry-run"
	} else {
		// the -f (force) option allows configuration only changes
		args = "-f"
	}

	if isInitNode {
		commands = append(commands,
			dialer.Command{
				Name:    "kubeadm-init-update-kubelet-args",
				Command: fmt.Sprintf(`sudo bash -c "kubeadm init phase kubelet-start --config %s"`, kubeadmYamlPath),
			},
			dialer.Command{
				Name:    "kubeadm-upgrade-apply-init-node",
				Command: fmt.Sprintf(`sudo bash -c "kubeadm upgrade apply --ignore-preflight-errors=all %s --config %s %s"`, args, kubeadmYamlPath, version),
			})
	} else {
		commands = append(commands,
			dialer.Command{
				Name:    "kubeadm-join-update-kubelet-args",
				Command: fmt.Sprintf(`sudo bash -c "kubeadm join phase kubelet-start --config %s"`, kubeadmYamlPath),
			},
			dialer.Command{
				Name:    "kubeadm-upgrade-apply-join-node",
				Command: fmt.Sprintf(`sudo bash -c "kubeadm upgrade apply --ignore-preflight-errors=all %s %s"`, args, version),
			})
	}

	if err := doCommands(n, commands); err != nil {
		return err
	}

	return nil
}

// UpgradeNode runs kubeadm upgrade node to update the kubelet configuration.
func UpgradeNode(n *dialer.Node, controlPlane bool, kubeadmYamlPath string, version string) error {
	log.Infof("[kubeadm] Running kubeadm upgrade node on [%s]", n.Address)

	var args string
	if controlPlane && version == "" {
		args = "experimental-control-plane"
	} else {
		args = fmt.Sprintf("config --kubelet-version %s", version)
	}
	commands := []dialer.Command{
		dialer.Command{
			Name:    "kubeadm-update-kubelet-args",
			Command: fmt.Sprintf(`sudo bash -c "kubeadm join phase kubelet-start --config %s"`, kubeadmYamlPath),
		},
		dialer.Command{
			Name:    "kubeadm-upgrade-node",
			Command: fmt.Sprintf(`sudo bash -c "kubeadm upgrade node %s"`, args),
		}}

	if err := doCommands(n, commands); err != nil {
		return err
	}
	return nil
}

func uploadYamlConfig(n *dialer.Node, yamlContents []byte, configPath string) error {
	log.Infof("[kubeadm] uploading the kubeadm config yaml")
	err := n.CopyFile(yamlContents, 0666, configPath)
	if err != nil {
		log.Fatalf("[kubeadm] Failed to upload kubeadm yaml to [%s] %s", n.Address, err)
	}
	return nil
}

func doCommands(n *dialer.Node, commands []dialer.Command) error {
	for _, command := range commands {
		log.Infof("[kubeadm] Running command %s", command.Name)
		err := n.Exec(command.Command)
		if err != nil {
			log.Errorf("[kubeadm] command %s failed. %s\n", command.Name, err)
			return err
		}
		log.Infof("[kubeadm] command %s completed", command.Name)
	}
	return nil
}

// etcd setup
func ResetEtcdDataCommand() string {
	return fmt.Sprintf("etcdctl del \"\" --prefix")
}

func KubeAdmInitEtcd() error {
	return nil
}
