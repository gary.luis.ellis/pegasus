package kubeadm

import (
	"crypto/x509"
	"errors"
	"fmt"
	"os"
	"path/filepath"

	"github.com/garyellis/pegasus/dialer"
	log "github.com/sirupsen/logrus"
	"k8s.io/client-go/tools/clientcmd"
	clientcertutil "k8s.io/client-go/util/cert"
	kubeconfigutil "k8s.io/kubernetes/cmd/kubeadm/app/util/kubeconfig"
	"k8s.io/kubernetes/cmd/kubeadm/app/util/pubkeypin"
)

// we should make a utility package of reusable functions and place makeWorkDir there.
// makeWorkDir creates the terraform root module directory
func makeWorkDir(path string) error {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		mode := 0755
		err := os.MkdirAll(path, os.FileMode(mode))
		if err != nil {
			log.Fatalln(err)
		}
	}
	return nil
}

func GetAdminKubeConf(n *dialer.Node, localKubeConf string) error {
	commands := map[string]dialer.Command{
		"copy-file": dialer.Command{
			Name:    "get admin kubeconfig",
			Command: fmt.Sprintf(`sudo bash -c "cp %s %s && chmod 666 %s"`, "/etc/kubernetes/admin.conf", "/tmp", "/tmp/admin.conf"),
		},
		"delete-tmpfile": dialer.Command{
			Name:    "delete-tempfile",
			Command: fmt.Sprintf(`sudo bash -c "rm -f %s"`, "/tmp/admin.conf"),
		},
	}

	copycmd := commands["copy-file"]
	log.Infof("[kubeadm/util] running command [%s]\n", copycmd.Name)
	err := n.Exec(copycmd.Command)
	if err != nil {
		log.Errorf("[%s] failed. %s", copycmd.Name, err)
		log.Fatalln(err)
	}
	log.Infof("[kubeadm/util] [%s] completed", copycmd.Name)

	// download the file
	workdir := filepath.Dir(localKubeConf)
	makeWorkDir(workdir)

	err = n.DownloadFile("/tmp/admin.conf", localKubeConf)
	if err != nil {
		log.Fatalln(err)
	}

	// delete the tmp file
	cleancmd := commands["delete-tmpfile"]
	log.Infof("[kubeadm/util] running command [%s]\n", cleancmd.Name)
	err = n.Exec(cleancmd.Command)
	if err != nil {
		log.Errorf("[util] [%s] failed. %s\n", cleancmd.Name, err)
		log.Fatalln(err)
	}
	log.Printf("[kubeadm/util] [%s] completed", cleancmd.Name)
	return nil
}

// We need to add a check for IsControlPlanePresent and do filesystem checks
// IsControlPlaneRunning returns true if control plane is running on the host.
func IsControlPlaneRunning(n *dialer.Node) bool {
	log.Infof("[kubeadm/util] Checking if control plane is running on [%s]", n.Address)
	cmdstr := fmt.Sprintf(`sudo bash -c "kubectl --kubeconfig /etc/kubernetes/admin.conf get nodes -o wide|egrep \"($HOSTNAME|%s)\""`, n.Address)
	isrunningcmd := dialer.Command{
		Name:    "check-control-plane-running",
		Command: cmdstr,
	}
	log.Infof("[kubeadm/util] running command [%s]\n", isrunningcmd.Name)
	err := n.Exec(isrunningcmd.Command)
	if err != nil {
		log.Infof("[util] [%s] Control plane is not running.", isrunningcmd.Name)
		return false
	}

	log.Infof("[kubeadm/util] Control plane is running on [%s].", n.Address)
	return true
}

// GetCACertHash populates ca cert pin for kubeadm join commands
func GetCACertHash(kubeconfig string) ([]string, error) {

	config, err := clientcmd.LoadFromFile(kubeconfig)
	if err != nil {
		log.Errorf("[kubeadm/util] failed to load kubeconfig", err)
	}
	clusterConfig := kubeconfigutil.GetClusterFromKubeConfig(config)
	if clusterConfig == nil {
		log.Errorf("[kubeadm/util] failed to get cluster config")
		return nil, errors.New("[kubeadm] failed to get cluster config")
	}

	var caCerts []*x509.Certificate
	if clusterConfig.CertificateAuthorityData != nil {
		caCerts, err = clientcertutil.ParseCertsPEM(clusterConfig.CertificateAuthorityData)
		if err != nil {
			log.Errorf("[kubeadm/util] failed to load ca certificate from kubeconfig")
			return nil, errors.New("[kubeadm] failed to load ca certificate from kubeconfig")
		}
	}

	publicKeyPins := make([]string, 0, len(caCerts))
	for _, caCert := range caCerts {
		publicKeyPins = append(publicKeyPins, pubkeypin.Hash(caCert))
	}

	return publicKeyPins, nil
}

func GetApiServer(kubeconfig string) (string, error) {
	config, err := clientcmd.LoadFromFile(kubeconfig)
	if err != nil {
		log.Errorf("[kubeadm/util] failed to load kubeconfig", err)
		return "", errors.New("[kubeadm] failed to kubeconfig")
	}
	clusterConfig := kubeconfigutil.GetClusterFromKubeConfig(config)
	if clusterConfig == nil {
		log.Errorf("[kubeadm/util] failed to get cluster config")
		return "", errors.New("[kubeadm] failed to get cluster config")
	}
	return clusterConfig.Server, nil
}

func installKubernetesPackages() string {
	return `
	`
}
