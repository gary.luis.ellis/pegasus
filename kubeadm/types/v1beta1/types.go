package types

// kubeadm v1beta1 configuration

type ClusterConfiguration struct {
	KubernetesVersion    string                `mapstructure:"kubernetesVersion,omitempty" json:"kubernetesVersion,omitempty"`
	ImageRepository      string                `mapstructure:"imageRepository,omitempty" json:"imageRepository,omitempty"`
	ClusterName          string                `mapstructure:"clusterName"`
	ControlPlaneEndpoint string                `mapstructure:"controlPlaneEndpoint"`
	Etcd                 Etcd                  `mapstructure:"etcd,omitempty" json:"etcd,omitempty"`
	Networking           Networking            `mapstructure:"networking,omitempty" json:"networking,omitempty"`
	DNS                  DNS                   `mapstructure:"dns,omitempty" json:"dns"`
	APIServer            APIServer             `mapstructure:"apiServer,omitempty" json:"apiServer,omitempty"`
	ControllerManager    ControlPlaneComponent `mapstructure:"controllerManager,omitempty" json:"controllerManager,omitempty"`
	Scheduler            ControlPlaneComponent `mapstructure:"scheduler,omitempty" json:"scheduler,omitempty"`

	// ControlPlaneKubelet is a user defined map of kubelet extra args applied to control plane nodes
	ControlPlaneKubelet Kubelet `mapstructure:"controlPlaneKubelet,omitempty"`

	// Kubelet stores a user defined map of kubelet extra args to set on all cluster nodes
	Kubelet Kubelet `mapstructure:"kubelet,omitempty" json:"kubelet,omitempty"`
}

type Kubelet struct {
	ExtraArgs map[string]string `mapstructure:"extraArgs,omitempty" json:"extraArgs,omitempty"`
}

type APIServer struct {
	CertSANs              []string `mapstructure:"certSANs,omitempty" json:"certSANs,omitempty"`
	ControlPlaneComponent `mapstructure:",squash,omitempty" json:",inline"`
	CertificatesDir       string `mapstructure:"certificatesDir,omitempty" json:"certificatesDir,omitempty"`
	ImageRepository       string `mapstructure:"imageRepository,omitempty" json:"imageRepository,omitempty"`
	//UseHyperKubeImage     bool            `mapstructure:"useHyperKubeImagejson:"useHyperKubeImage,omitempty"`
	FeatureGates map[string]bool `mapstructure:"featureGates,omitempty" json:"featureGates,omitempty"`
	ClusterName  string          `mapstructure:"clusterName" json:"clusterName,omitempty"`
}

type Etcd struct {
	Local    LocalEtcd    `mapstructure:"local,omitempty"`
	External ExternalEtcd `mapstructure:"external,omitempty"`
}

type LocalEtcd struct {
	ImageMeta      `mapstructure:",squash,omitempty" json:",inline"`
	DataDir        string            `mapstructure:"dataDir,omitempty" json:"dataDir"`
	ExtraArgs      map[string]string `mapstructure:"extraArgs,omitempty" json:"extraArgs,omitempty"`
	ServerCertSANs []string          `mapstructure:"serverCertSANs,omitempty" json:"serverCertSANs,omitempty"`
	PeerCertSANs   []string          `mapstructure:"peerCertSANs,omitempty" json:"peerCertSANs,omitempty"`
}

type ExternalEtcd struct {
	Endpoints []string `mapstructure:"endpoints,omitempty" json:"endpoints"`
	CAFile    string   `mapstructure:"caFile,omitempty" json:"caFile"`
	CertFile  string   `mapstructure:"certFile,omitempty" json:"certFile"`
	KeyFile   string   `mapstructure:"keyFile" json:"keyFile"`
}

type Networking struct {
	ServiceSubnet string `mapstructure:"serviceSubnet,omitempty" json:"serviceSubnet,omitempty"`
	PodSubnet     string `mapstructure:"podSubnet,omitempty" json:"podSubnet"`
	DNSDomain     string `mapstructure:"dnsDomain,omitempty" json:"dnsDomain"`
}

type ControlPlaneComponent struct {
	ExtraArgs    map[string]string `mapstructure:"extraArgs,omitempty" json:"extraArgs,omitempty"`
	ExtraVolumes []HostPathMount   `mapstructure:"extraVolumes,omitempty" json:"extraVolumes,omitempty"`
}

type HostPathMount struct {
	Name      string `mapstructure:"name,omitempty" json:"name"`
	HostPath  string `mapstructure:"hostPath,omitempty" json:"hostPath"`
	MountPath string `mapstructure:"mountPath,omitempty" json:"mountPath"`
	ReadOnly  bool   `mapstructure:"readOnly,omitempty" json:"readOnly,omitempty"`
	PathType  string `mapstructure:"pathType,omitempty" json:"pathType,omitempty"`
}

type DNS struct {
	Type      string    `mapstructure:"type,omitempty" json:"type"`
	ImageMeta ImageMeta `mapstructure:",inline,omitempty" json:",inline"`
}

type ImageMeta struct {
	ImageRepository string `mapstructure:"imageRepository,omitempty" json:"imageRepository,omitempty"`
	ImageTag        string `mapstructure:"imageTag,omitempty" json:"imageTag,omitempty"`
}

// Init Configuration
type InitConfiguration struct {
	BootstrapTokens  []BootstrapToken        `mapstructure:"bootstrapTokens,omitempty"`
	NodeRegistration NodeRegistrationOptions `mapstructure:"nodeRegistration,omitempty"`
	LocalAPIEndpoint APIEndpoint             `mapstructure:"localAPIEndpoint,omitempty"`
}

type BootstrapToken struct {
	Token       string   `mapstructure:"token,omitempty"`
	Description string   `mapstructure:"description,omitempty"`
	TTL         string   `mapstructure:"ttl,omitempty"`
	Usages      []string `mapstructure:"usages,omitempty"`
	Groups      []string `mapstructure:"groups,omitempty"`
}

type NodeRegistrationOptions struct {
	Name             string            `mapstructure:"name,omitempty"`
	CRISocket        string            `mapstructure:"criSocket,omitempty"`
	Taints           []Taint           `mapstructure:"taints,omitempty"`
	KubeletExtraArgs map[string]string `mapstructure:"kubeletExtraArgs,omitempty"`
}

type Taint struct {
	Key string `json:"key" protobuf:"bytes,1,opt,name=key"`

	Value  string      `mapstructure:"value,omitempty"`
	Effect TaintEffect `mapstructure:"effect,omitempty"`
}

type TaintEffect string

type APIEndpoint struct {
	AdvertiseAddress string `mapstructure:"advertiseAddress"`
	BindPort         string `mapstructure:"bindPort"`
}

// Join configuration
