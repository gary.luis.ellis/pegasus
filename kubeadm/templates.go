package kubeadm

import (
	"bytes"
	"html/template"
)

const kubeadmInitEtcdTmpl = `
---
apiVersion: "kubeadm.k8s.io/v1beta1"
kind: ClusterConfiguration
kubernetesVersion: "{{.KubernetesVersion}}"
controlPlaneEndpoint: "{{.ControlPlaneEndpoint}}"
clusterName: "{{.ClusterName}}"
# etcd setup
`

const kubeletEtcdDropInFile = `
[Service]
ExecStart=
ExecStart=/usr/bin/kubelet --address=127.0.0.1 --pod-manifest-path=/etc/kubernetes/manifests --allow-privileged=true
Restart=always
`

const initControlPlaneTmpl = `---
apiVersion: "kubeadm.k8s.io/v1beta1"
kind: ClusterConfiguration
{{- with .ClusterConfig.KubernetesVersion}}
kubernetesVersion: {{.}}
{{- end}}
{{- with .ClusterConfig.ImageRepository}}
imageRepository: "{{.}}"
{{- end}}

clusterName: {{ .ClusterConfig.ClusterName}}
controlPlaneEndpoint: {{ .ClusterConfig.ControlPlaneEndpoint}}

networking:
{{- with .ClusterConfig.Networking.PodSubnet}}
  podSubnet: "{{.}}"
{{- end}}
{{- with .ClusterConfig.Networking.ServiceSubnet}}
  serviceSubnet: "{{.}}"
{{- end}}

{{- with .ClusterConfig.Networking.DNSDomain}}
  dnsDomain: "{{.}}"
{{- end}}

dns:
{{- with .ClusterConfig.DNS.Type}}
  type: {{.}}
{{- end}}

etcd:
{{- with .ClusterConfig.Etcd.Local}}
  local:
    imageRepository: "{{.ImageRepository}}"
    imageTag: "{{.ImageTag}}"
    extraArgs:
    {{- range $k, $v := .ExtraArgs}}
      {{ $k }}: "{{ $v -}}"
    {{- end}}
{{- end}}

apiServer:
  certSANs:
  {{- range .ControlPlaneNodes }}
    - "{{.Address -}}"
  {{- end}}
  {{- range .ControlPlaneNodes }}
    - "{{.HostName -}}"
  {{- end }}
  {{- range .ClusterConfig.APIServer.CertSANs}}
    - "{{.}}"
  {{- end}}

  extraArgs:
  {{- range $k, $v := .ClusterConfig.APIServer.ExtraArgs}}
    {{ $k }}: "{{ $v -}}"
  {{- end }}

  extraVolumes:
  {{- range .ClusterConfig.APIServer.ExtraVolumes}}
    - name: {{.Name }}
      hostPath: {{.HostPath}}
      mountPath: {{.MountPath}}
      readOnly: {{.ReadOnly}}
      pathType: {{.PathType}}
  {{- end }}

controllerManager:
  extraArgs:
  {{- range $k, $v := .ClusterConfig.ControllerManager.ExtraArgs}}
    {{ $k }}: "{{ $v -}}"
  {{- end }}

  extraVolumes:
  {{- range .ClusterConfig.ControllerManager.ExtraVolumes}}
    - name: {{.Name }}
      hostPath: {{.HostPath}}
      mountPath: {{.MountPath}}
      readOnly: {{.ReadOnly}}
      pathType: {{.PathType}}
  {{- end }}

scheduler:
  extraArgs:
  {{- range $k, $v := .ClusterConfig.Scheduler.ExtraArgs}}
    {{ $k }}: "{{ $v -}}"
  {{- end }}

  extraVolumes:
  {{- range .ClusterConfig.Scheduler.ExtraVolumes}}
    - name: {{.Name }}
      hostPath: {{.HostPath}}
      mountPath: {{.MountPath}}
      readOnly: {{.ReadOnly}}
      pathType: {{.PathType}}
  {{- end }}

---
apiVersion: "kubeadm.k8s.io/v1beta1"
kind: InitConfiguration
bootstrapTokens:
{{- range .InitConfig.BootstrapTokens}}
- token: {{.Token }}
  description: "{{.Description}}"
  ttl: "{{.TTL}}"
  {{- range .Usages}}
  usages:
    - {{. -}}
  {{- end}}
  {{- range .Groups}}
  groups:
    - {{.}}
  {{- end}}
{{- end}}

nodeRegistration:
  {{- if .ClusterConfig.Kubelet.ExtraArgs}}
  kubeletExtraArgs:
  {{- range $k, $v := .ClusterConfig.ControlPlaneKubelet.ExtraArgs}}
    {{ $k }}: "{{ $v -}}"
  {{- end}}
  {{- end}}

localAPIEndpoint:
  advertiseAddress: "{{.LocalControlPlaneNode.Address}}"
  {{- if .InitConfig.LocalAPIEndpoint.BindPort}}
  bindPort: {{.InitConfig.LocalAPIEndpoint.BindPort}}
  {{- end}}
`

const joinControlPlaneTmpl = `---
apiVersion: "kubeadm.k8s.io/v1beta1"
kind: JoinConfiguration

discovery:
  bootstrapToken:
    apiServerEndpoint: {{ .ClusterConfig.ControlPlaneEndpoint}}
  {{- with $i := index .InitConfig.BootstrapTokens 0 }}
    token: {{$i.Token}}
  {{- end}}
    caCertHashes:
    {{- range .CaCertHashes}}
      - {{.}}
    {{- end}}

controlPlane:
  localAPIEndpoint:
    advertiseAddress: "{{.LocalNode.Address}}"
    {{- if .InitConfig.LocalAPIEndpoint.BindPort}}
    bindPort: {{.InitConfig.LocalAPIEndpoint.BindPort}}
    {{- end}}

nodeRegistration:
  {{- if .ClusterConfig.Kubelet.ExtraArgs}}
  kubeletExtraArgs:
  {{- range $k, $v := .ClusterConfig.ControlPlaneKubelet.ExtraArgs}}
    {{ $k }}: "{{ $v -}}"
  {{- end}}
  {{- end}}
`

const joinWorkerTmpl = `---
apiVersion: "kubeadm.k8s.io/v1beta1"
kind: JoinConfiguration

discovery:
  bootstrapToken:
    apiServerEndpoint: {{ .ClusterConfig.ControlPlaneEndpoint}}
  {{- with $i := index .InitConfig.BootstrapTokens 0 }}
    token: {{$i.Token}}
  {{- end}}
    caCertHashes:
    {{- range .CaCertHashes}}
      - {{.}}
    {{- end}}

nodeRegistration:
  kubeletExtraArgs:
  {{- range $k, $v := .ClusterConfig.Kubelet.ExtraArgs}}
    {{ $k }}: "{{ $v -}}"
  {{- end }}
`

// GetInitControlPlaneTmpl returns a rendered kubeadm init template for the given accepts cluster configuration, init configuration, control plane nodes list and the current control plane node address
func GetInitControlPlaneTmpl(clusterConfig, initConfig, controlPlaneNodes, localControlPlaneNode interface{}) ([]byte, error) {
	InitConfig := map[string]interface{}{
		"ClusterConfig":         clusterConfig,
		"InitConfig":            initConfig,
		"ControlPlaneNodes":     controlPlaneNodes,
		"LocalControlPlaneNode": localControlPlaneNode,
	}

	return CompileTemplateFromMap(initControlPlaneTmpl, InitConfig)
}

// GetJoinControlPlaneTmpl returns join configuration yaml for a control plane node
func GetJoinControlPlaneYaml(clusterConfig, initConfig, localNode, caCertHashes interface{}) ([]byte, error) {
	joinConfig := map[string]interface{}{
		"ClusterConfig": clusterConfig,
		"InitConfig":    initConfig,
		"LocalNode":     localNode,
		"CaCertHashes":  caCertHashes,
	}
	return CompileTemplateFromMap(joinControlPlaneTmpl, joinConfig)
}

// GetJoinWorkerTmpl returns join configuration yaml for a worker node
func GetJoinWorkerYaml(clusterConfig, initConfig, localNode, caCertHashes interface{}) ([]byte, error) {
	joinConfig := map[string]interface{}{
		"ClusterConfig": clusterConfig,
		"InitConfig":    initConfig,
		"LocalNode":     localNode,
		"CaCertHashes":  caCertHashes,
	}
	return CompileTemplateFromMap(joinWorkerTmpl, joinConfig)
}

func CompileTemplateFromMap(tmplt string, input interface{}) ([]byte, error) {
	out := new(bytes.Buffer)
	t := template.Must(template.New("compiled_template").Parse(tmplt))
	if err := t.Execute(out, input); err != nil {
		return nil, err
	}
	return out.Bytes(), nil
}
