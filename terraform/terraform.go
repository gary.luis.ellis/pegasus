package terraform

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"regexp"

	"gopkg.in/yaml.v2"
	kyaml "sigs.k8s.io/yaml"

	terraformtypes "github.com/garyellis/pegasus/terraform/types"
	"github.com/garyellis/pegasus/util/shellutils"

	log "github.com/sirupsen/logrus"
)

const maintffile = "main.tf.json"

// Executor wraps terraform initialization and commands.
type Executor struct {
	WorkDir string
	Payload interface{}
}

// NewTerraformExecutor returns a TerraformExecutor to run terraform commands
func NewTerraformExecutor(workdir string, paylaod interface{}) *Executor {
	return &Executor{
		WorkDir: workdir,
		Payload: paylaod,
	}
}

// Init stages the terraform root module and runs the terraform init command.
func (t *Executor) Init() error {

	makeWorkDir(t.WorkDir)
	WriteTf(t.Payload, fmt.Sprintf("%s/%s", t.WorkDir, maintffile))

	_, err := t.terraform("init")
	if err != nil {
		log.Fatalf("[terraform] terraform init failed. %s", err)
	}
	return nil
}

// Apply runs terraform apply on the terraform work directory
func (t *Executor) Apply() error {
	_, err := t.terraform("apply", "-auto-approve")
	if err != nil {
		log.Fatalf("[terraform] terraform apply failed. %s", err)
	}
	return nil
}

// Plan will be implemented for gitops based infra changes/upgrade workflow and for infra status command.
func (t *Executor) Plan() error {
	_, err := t.terraform("plan")
	if err != nil {
		log.Fatalf("[terraform] terraform plan failed. %s", err)
	}
	return nil
}

// terraform wraps the terraform binary
func (t *Executor) terraform(args ...string) ([]byte, error) {
	log.Infof("[terraform] running terraform %s", args)

	cmd := exec.Command("terraform", args...)
	cmd.Env = os.Environ()
	cmd.Dir = t.WorkDir

	var returnStdout, returnStderr []byte
	cmdStdOutReader, _ := cmd.StdoutPipe()
	cmdStdErrReader, _ := cmd.StderrPipe()

	err := cmd.Start()
	if err != nil {
		return nil, err
	}

	returnStdout, _ = shellutils.ReadStdOutErrPipe(cmdStdOutReader)
	returnStderr, _ = shellutils.ReadStdOutErrPipe(cmdStdErrReader)
	err = cmd.Wait()
	if err != nil {
		log.Errorf("[terraform] %s", string(returnStderr))
		return nil, err
	}

	return returnStdout, nil
}

// makeWorkDir creates the terraform root module directory
func makeWorkDir(path string) error {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		mode := 0755
		err := os.MkdirAll(path, os.FileMode(mode))
		if err != nil {
			log.Fatalln(err)
		}
	}
	return nil
}

// WriteTf writes the terraform configuration payload to the root module directory
func WriteTf(payload interface{}, path string) error {

	// let yaml.v2 convert dynamic types map[interface{}]interface{} to
	// map[string]interface{}, and []interface{} automatically prior to marshal to json.
	tfyaml, err := yaml.Marshal(payload)
	if err != nil {
		log.Errorf("[terraform] yaml marshal failed. %s", err)
	}
	tfjson, err := kyaml.YAMLToJSON(resolveModuleRelativePaths(tfyaml))

	if err != nil {
		log.Fatalf("[terraform] json marshal failed. %s", err)
	}

	err = ioutil.WriteFile(path, tfjson, 0644)
	if err != nil {
		log.Fatalf("[terraform] write %s failed. %s\n", path, err)
	}
	return nil
}

// Output runs the terraform output command.
func (t *Executor) Output() ([]byte, error) {
	b, err := t.terraform("output", "-json")
	if err != nil {
		log.Fatalf("[terraform] terraform output command failed. %s", err)
	}
	return b, nil
}

// Destroy runs terraform destroy on the cluster infra
func (t *Executor) Destroy() error {
	_, err := t.terraform("destroy", "-auto-approve")
	if err != nil {
		log.Fatalf("[terraform] terraform destroy failed. %s", err)
	}
	// Need to return error to caller
	return nil
}

func resolveModuleRelativePaths(b []byte) []byte {
	re := regexp.MustCompile(`(source:[\s]+[\"]*)\./(.*[\"]*[\n])`)
	return re.ReplaceAll(b, []byte("$1../../$2"))
}

func GetExecutor(workdir string, module terraformtypes.RootModule) (*Executor, error) {
	tfexecutor := NewTerraformExecutor(workdir, module)
	if err := tfexecutor.Init(); err != nil {
		return nil, err
	}
	return tfexecutor, nil
}
