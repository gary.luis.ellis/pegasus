package terraform

type Terraform struct {
	Image   string       `mapstructure:"image" json:"image"`
	Modules []RootModule `mapstructure:"modules" json:"modules"`
}

type RootModule struct {
	Provider       Provider       `mapstructure:"provider" yaml:"provider,omitempty" json:"provider,omitempty"`
	DataBlock      DataBlock      `mapstructure:"data" yaml:"data,omitempty" json:"data,omitempty"`
	Resource       Resource       `mapstructure:"resource" yaml:"resource,omitempty" json:"resource,omitempty"`
	Module         Module         `mapstructure:"module" yaml:"module,omitempty" json:"module,omitempty"`
	Variable       Variable       `mapstructure:"variable" yaml:"variable,omitempty" json:"variable,omitempty"`
	Output         Output         `mapstructure:"output" yaml:"output,omitempty" json:"output,omitempty"`
	TerraformBlock TerraformBlock `mapstructure:"terraform" yaml:"terraform,omitempty" json:"terraform,omitempty"`
}

type DataBlock map[string]interface{}

type TerraformBlock map[string]interface{}

type Provider map[string]interface{}

type Resource map[string]interface{}

type Module map[string]interface{}

type Variable map[string]VariableItem

type VariableItem struct {
	Type    string      `mapstructure:"type" yaml:"type,omitempty" json:"type,omitempty"`
	Default interface{} `mapstructure:"default" yaml:"default,omitempty" json:"default,omitempty"`
}

type Output map[string]interface{}
