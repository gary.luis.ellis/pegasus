package terraform

import (
	"encoding/json"
	"fmt"
	"strings"

	"gopkg.in/yaml.v2"

	"github.com/garyellis/pegasus/dialer"
	terraformtypes "github.com/garyellis/pegasus/terraform/types"
	log "github.com/sirupsen/logrus"
)

const (
	etcdWorkDir             = "./tmp/etcd"
	etcdNodesOutput         = "etcd_nodes"
	controlPlaneWorkDir     = "./tmp/control-plane"
	controlPlaneNodesOutput = "control_plane_nodes"
	workerDir               = "./tmp/worker"
	workerNodesOutput       = "worker_nodes"
)

// Plan runs terraform plan on the input terraform modules
func Plan(modules []terraformtypes.RootModule) error {
	log.Infof("[terraform/infra] validating terraform infra plan")
	for i, rootModule := range modules {
		workdir := fmt.Sprintf("./tmp/%d", i)
		tfexecutor := NewTerraformExecutor(workdir, rootModule)
		log.Infof("[terraform/infra] terraform init ")
		err := tfexecutor.Init()
		if err != nil {
			log.Errorf("[terraform/infra] %s", err)
			return err
		}
		log.Infof("[terraform/infra] terraform plan")
		err = tfexecutor.Plan()
		if err != nil {
			log.Errorf("[terraform/infra] %s", err)
			return err
		}
	}
	return nil
}

// Create applies the input terraform modules and returns etcd, control plane and worker nodes
func Create(modules []terraformtypes.RootModule) ([]dialer.Node, []dialer.Node, []dialer.Node, error) {
	log.Infof("[terraform/infra] creating infrastructure")

	var etcdNodes []dialer.Node
	var controlPlaneNodes []dialer.Node
	var workerNodes []dialer.Node

	for i, rootModule := range modules {
		workdir := fmt.Sprintf("./tmp/%d", i)
		tfexecutor := NewTerraformExecutor(workdir, rootModule)
		log.Infof("[terraform/infra] terraform init ")
		err := tfexecutor.Init()
		if err != nil {
			log.Errorf("[infra] %s", err)
			return nil, nil, nil, err
		}
		err = tfexecutor.Apply()
		if err != nil {
			log.Errorf("[terraform/infra] terraform apply failed. %s", err)
			return nil, nil, nil, err
		}

		// get the etcd nodes
		mEtcdNodes, err := GetNodesFromOutput(tfexecutor, etcdNodesOutput)
		if err != nil {
			log.Errorf("[terraform/infra] get control plane nodes failed. %s", err)
			return nil, nil, nil, err
		}

		// get the control plane nodes
		log.Infof("[terraform/infra] setting control plane nodes")
		mControlPlaneNodes, err := GetNodesFromOutput(tfexecutor, controlPlaneNodesOutput)
		if err != nil {
			log.Errorf("[terraform/infra] get control plane nodes failed. %s", err)
			return nil, nil, nil, err
		}

		// setup the worker nodes
		log.Infof("[terraform/infra] setting worker nodes")
		mWorkerNodes, err := GetNodesFromOutput(tfexecutor, workerNodesOutput)
		if err != nil {
			log.Errorf("[terraform/infra] get worker nodes failed. %s", err)
			return nil, nil, nil, err
		}

		etcdNodes = append(etcdNodes, mEtcdNodes...)
		controlPlaneNodes = append(controlPlaneNodes, mControlPlaneNodes...)
		workerNodes = append(workerNodes, mWorkerNodes...)
	}
	return etcdNodes, controlPlaneNodes, workerNodes, nil
}

func Destroy(modules []terraformtypes.RootModule) error {
	log.Infof("[terraform/infra] Destroying infrastructure")
	for i, rootModule := range modules {
		workdir := fmt.Sprintf("./tmp/%d", i)
		tfexecutor := NewTerraformExecutor(workdir, rootModule)
		log.Infof("[terraform/infra] terraform init ")
		err := tfexecutor.Init()
		if err != nil {
			log.Errorf("[terraform/infra] %s", err)
			return err
		}
		err = tfexecutor.Destroy()
		if err != nil {
			log.Errorln(err)
			return err
		}
	}
	return nil
}

// WIP
func Output(modules []terraformtypes.RootModule) error {
	for i, rootModule := range modules {
		workdir := fmt.Sprintf("./tmp/%d", i)
		tfexecutor, err := GetExecutor(workdir, rootModule)
		if err != nil {
			return err
		}

		var outputs OutputData
		o, err := tfexecutor.Output()

		err = yaml.Unmarshal(o, &outputs)
		if err != nil {
			return err
		}
		yamlOutputs, err := yaml.Marshal(outputs)
		if err != nil {
			return err
		}
		log.Info("\n", string(yamlOutputs))
	}
	return nil
}

type OutputData map[string]OutputItem

type OutputItem struct {
	Value []dialer.Node `json:"value"`
}

func GetNodesFromOutput(t *Executor, outputName string) ([]dialer.Node, error) {
	outputdata := OutputData{}
	b, err := t.Output()
	if err != nil {
		log.Errorln(err)
		return nil, err
	}
	err = json.Unmarshal(b, &outputdata)
	if err != nil {
		log.Errorln(err)
		return nil, err
	}

	nodes := []dialer.Node{}
	if _, ok := outputdata[outputName]; ok {
		nodes = outputdata[outputName].Value
	}
	var printNodes []string
	for _, node := range nodes {
		printNodes = append(printNodes, fmt.Sprintf("%s@%s", node.User, node.Address))
	}
	log.Infof("[terraform/infra] output [%s] got nodes [%s]", outputName, strings.Join(printNodes, ", "))
	return nodes, nil
}

// placeholder to create terraform maintenance workflows to change or replace cluster nodes.
func upgradeControlPlaneNode() error {
	return nil
}
func replaceControlPlaneNode() error {
	return nil
}
